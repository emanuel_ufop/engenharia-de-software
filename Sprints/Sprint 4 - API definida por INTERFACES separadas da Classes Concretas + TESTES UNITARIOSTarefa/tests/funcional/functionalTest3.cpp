#include "functionalTest3.hpp"

/**
  * \param source is the quantity of the Flow's source
  *
  * \param target is the quantity of the Flow's target
  *
  * \return float
  *
  **/
float myFlow3::execute (float source, float target)
{
	return source * 0.01;
}

/**
  * \return void
  */
void test3 ()
{
	ModelImpl *m3 = new ModelImpl;
	myFlow3 *v = new myFlow3, *f = new myFlow3, *g = new myFlow3, *t = new myFlow3, *u = new myFlow3, *r = new myFlow3;
	SystemImpl *Q1 = new SystemImpl, *Q2 = new SystemImpl, *Q3 = new SystemImpl, *Q4 = new SystemImpl, *Q5 = new SystemImpl;

	m3->add(f);
	m3->add(g);
	m3->add(t);
	m3->add(u);
	m3->add(r);
	m3->add(v);
	m3->add(Q1);
	m3->add(Q2);
	m3->add(Q3);
	m3->add(Q4);
	m3->add(Q5);

	v->setId("v");
	f->setId("f");
	g->setId("g");
	t->setId("t");
	u->setId("u");
	r->setId("r");

	Q1->setId("Q1");
	Q2->setId("Q2");
	Q3->setId("Q3");
	Q4->setId("Q4");
	Q5->setId("Q5");

	Q1->setQuantity(100);
	Q2->setQuantity(0);
	Q3->setQuantity(100);
	Q4->setQuantity(0);
	Q5->setQuantity(0);

	v->connect(Q4, 0); v->connect(Q1, 1);
	f->connect(Q1, 0); f->connect(Q2, 1);
	g->connect(Q1, 0); g->connect(Q3, 1);
	t->connect(Q2, 0); t->connect(Q3, 1);
	u->connect(Q3, 0); u->connect(Q4, 1);
	r->connect(Q2, 0); r->connect(Q5, 1);

	m3->simulate(0, 100);

	//m3->show();

	assert(fabs(Q1->getQuantity() - 31.8513) < 0.0001);
	assert(fabs(Q2->getQuantity() - 18.4002) < 0.0001);
	assert(fabs(Q3->getQuantity() - 77.1143) < 0.0001);
	assert(fabs(Q4->getQuantity() - 56.1728) < 0.0001);
	assert(fabs(Q5->getQuantity() - 16.4612) < 0.0001);
}