#include "functionalTest2.hpp"

/**
  * \param source is the quantity of the Flow's source
  *
  * \param target is the quantity of the Flow's target
  *
  * \return float
  *
  **/
float myFlow2::execute (float source, float target)
{
	return 0.01 * target * (1 - target / 70);
}

/**
  * \return void
  */
void test2 ()
{
	ModelImpl *m2 = new ModelImpl;
	myFlow2 *logistica = new myFlow2;
	SystemImpl *p1 = new SystemImpl, *p2 = new SystemImpl;


	m2->add(logistica);

	m2->add(p1);
	m2->add(p2);

	p1->setId("p1");
	p2->setId("p2");

	p1->setQuantity(100);
	p2->setQuantity(10);

	logistica->connect(p1, 0); logistica->connect(p2, 1);

	m2->simulate(0, 100);

	//m2->show();

	assert(fabs(p1->getQuantity() - 88.2167) < 0.0001);
	assert(fabs(p2->getQuantity() - 21.7833) < 0.0001);
}