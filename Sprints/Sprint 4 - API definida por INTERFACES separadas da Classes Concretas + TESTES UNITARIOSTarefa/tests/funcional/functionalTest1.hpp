#ifndef functionalTest1_HPP
#define functionalTest1_HPP

#include <math.h>
#include <assert.h>

#include "../../src/modelImpl.hpp"
#include "../../src/flowImpl.hpp"
#include "../../src/systemImpl.hpp"

using namespace std;

class myFlow1 : public FlowImpl
{
	/** Define the equation to the Flow */
	float execute (float, float);
};

/** This is a functional test*/
void test1 ();

#endif