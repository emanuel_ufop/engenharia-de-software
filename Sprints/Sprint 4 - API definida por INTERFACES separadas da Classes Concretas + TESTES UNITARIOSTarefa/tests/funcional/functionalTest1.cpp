#include "functionalTest1.hpp"

/**
  * \param source is the quantity of the Flow's source
  *
  * \param target is the quantity of the Flow's target
  *
  * \return float
  *
  **/
float myFlow1::execute (float source, float target)
{
	return source * 0.01;
}

/**
  * \return void
  */
void test1()
{
	myFlow1 *exponencial = new myFlow1;
	SystemImpl *pop1 = new SystemImpl, *pop2 = new SystemImpl;
	ModelImpl *m = new ModelImpl;

	m->add(exponencial);
	m->add(pop1);
	m->add(pop2);

	exponencial->setId("exponencial");
	pop1->setId("pop1");
	pop2->setId("pop2");

	pop1->setQuantity(100);
	pop2->setQuantity(0);

	exponencial->connect(pop1, 0); exponencial->connect(pop2, 1);

	m->simulate(0, 100);

	// m->show();

	assert(fabs(pop1->getQuantity() - 36.6032) < 0.0001);
	assert(fabs(pop2->getQuantity() - 63.3968) < 0.0001);
}