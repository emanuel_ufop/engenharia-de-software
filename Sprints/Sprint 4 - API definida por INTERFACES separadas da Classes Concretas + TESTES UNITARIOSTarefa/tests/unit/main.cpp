/**
* @file main.cpp
* @author Emanuel Jesus Xavier
* @date September 24, 2019
* @brief This program is a unit test for a dynamics system simulation
*
*/

#include "unitTestSystem.hpp"
#include "unitTestFlow.hpp"
#include "unitTestModel.hpp"

int main ()
{
	unitTestSystem_global ();
	unitTestFlow_global ();
	unitTestModel_global ();

	return 0;
}