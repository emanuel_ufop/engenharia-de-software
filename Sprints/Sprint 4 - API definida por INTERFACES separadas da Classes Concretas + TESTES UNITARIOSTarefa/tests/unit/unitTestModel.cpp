#include "unitTestModel.hpp"

class myModel : public ModelImpl
{
public:
	bool search_flow(FlowImpl *f)
	{
		for(vector<FlowImpl*>::iterator it = this->flowImpl_vector.begin(); it != flowImpl_vector.end(); it++)
			if((*it) == f)
				return true;

		return false;
	}

	bool search_system(SystemImpl *s)
	{
		for(vector<SystemImpl*>::iterator it = this->systemImpl_vector.begin(); it != systemImpl_vector.end(); it++)
			if((*it) == s)
				return true;

		return false;
	}
};

class myFlow1 : public FlowImpl
{
	float execute (float source, float target)
	{
		return source * 0.01;
	}
};

void unitTestModel_constructor ()
{
	myFlow1 *exponencial = new myFlow1;
	SystemImpl ss1(2, "asd"), ss2;
	SystemImpl *pop1 = &ss1, *pop2 = &ss2;
	ModelImpl *m = new ModelImpl;

	m->add(exponencial);
	m->add(pop1);
	m->add(pop2);

	exponencial->setId("exponencial");
	pop1->setId("pop1");
	pop2->setId("pop2");

	pop1->setQuantity(100);
	pop2->setQuantity(0);

	exponencial->connect(pop1, 0); exponencial->connect(pop2, 1);

	ModelImpl *m1 = new ModelImpl(m);

	m1->simulate(0, 100);


	assert(fabs(pop1->getQuantity() - 36.6032) < 0.0001);
	assert(fabs(pop2->getQuantity() - 63.3968) < 0.0001);

	delete exponencial;
	delete m;
}

void unitTestModel_add_flow () 
{
	myModel *m = new myModel();
	FlowImpl *f1 = new FlowImpl(), *f2 = new FlowImpl();

	m->add(f1);
	m->add(f2);

	assert(m->search_flow(f1));
	assert(m->search_flow(f2));

	delete m;
	delete f1;
	delete f2;
}

void unitTestModel_add_system () 
{
	myModel *m = new myModel();
	SystemImpl ss1(2, "asd"), ss2;
	SystemImpl *s1 = &ss1;
	SystemImpl *s2 = &ss2;

	m->add(s1);
	m->add(s2);

	assert(m->search_system(s1));
	assert(m->search_system(s2));

	delete m;
}

void unitTestModel_remove_flow () 
{
	myModel *m = new myModel();
	FlowImpl *f1 = new FlowImpl(), *f2 = new FlowImpl();

	m->add(f1);
	m->add(f2);

	m->remove(f1);
	m->remove(f2);

	assert(!m->search_flow(f1));
	assert(!m->search_flow(f2));

	delete m;
	delete f1;
	delete f2;
}

void unitTestModel_remove_system () 
{
	myModel *m = new myModel();
	SystemImpl ss1(2, "asd"), ss2;
	SystemImpl *s1 = &ss1;
	SystemImpl *s2 = &ss2;

	m->add(s1);
	m->add(s2);

	m->remove(s1);
	m->remove(s2);

	assert(!m->search_system(s1));
	assert(!m->search_system(s2));

	delete m;
}

void unitTestModel_simulate () 
{
	myFlow1 *exponencial = new myFlow1;
	SystemImpl ss1(2, "asd"), ss2;
	SystemImpl *pop1 = &ss1, *pop2 = &ss2;
	ModelImpl *m = new ModelImpl;

	m->add(exponencial);
	m->add(pop1);
	m->add(pop2);

	exponencial->setId("exponencial");
	pop1->setId("pop1");
	pop2->setId("pop2");

	pop1->setQuantity(100);
	pop2->setQuantity(0);

	exponencial->connect(pop1, 0); exponencial->connect(pop2, 1);

	m->simulate(0, 100);


	assert(fabs(pop1->getQuantity() - 36.6032) < 0.0001);
	assert(fabs(pop2->getQuantity() - 63.3968) < 0.0001);

	delete exponencial;
	delete m;
}

void unitTestModel_operator_attribution ()
{
	myFlow1 *exponencial = new myFlow1;
	SystemImpl ss1(2, "asd"), ss2;
	SystemImpl *pop1 = &ss1, *pop2 = &ss2;
	ModelImpl *m = new ModelImpl;

	m->add(exponencial);
	m->add(pop1);
	m->add(pop2);

	exponencial->setId("exponencial");
	pop1->setId("pop1");
	pop2->setId("pop2");

	pop1->setQuantity(100);
	pop2->setQuantity(0);

	exponencial->connect(pop1, 0); exponencial->connect(pop2, 1);

	ModelImpl *m2 = new ModelImpl;

	m2 = m;

	assert(m == m2);

	delete exponencial;
	delete m;
}

void unitTestModel_operator_compare ()
{
	myFlow1 *exponencial = new myFlow1;
	SystemImpl ss1(2, "asd"), ss2;
	SystemImpl *pop1 = &ss1, *pop2 = &ss2;
	ModelImpl *m = new ModelImpl;

	m->add(exponencial);
	m->add(pop1);
	m->add(pop2);

	exponencial->setId("exponencial");
	pop1->setId("pop1");
	pop2->setId("pop2");

	pop1->setQuantity(100);
	pop2->setQuantity(0);


	exponencial->connect(pop1, 0); exponencial->connect(pop2, 1);

	ModelImpl *m2 = new ModelImpl;

	m2->add(exponencial);
	m2->add(pop1);
	m2->add(pop2);

	assert(m == m);

	delete exponencial;
	delete m;
	delete m2;
}

void unitTestModel_global ()
{
	unitTestModel_constructor ();
	unitTestModel_add_flow (); 
	unitTestModel_add_system (); 
	unitTestModel_remove_flow (); 
	unitTestModel_remove_system ();
	unitTestModel_simulate ();
	unitTestModel_operator_attribution ();
	unitTestModel_operator_compare ();
}
