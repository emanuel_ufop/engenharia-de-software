#include "unitTestFlow.hpp"

void unitTestFlow_construtor1 ()
{
	FlowImpl *flow = new FlowImpl();

	assert(flow->getTarget() == NULL);
	assert(flow->getSource() == NULL);

	delete flow;
}

void unitTestFlow_construtor2 ()
{
	SystemImpl ss1(2, "asd"), ss2;
	SystemImpl *s1 = &ss1;
	SystemImpl *s2 = &ss2;
	FlowImpl *flow = new FlowImpl("f1", s1, s2);

	assert(flow->getSource() == s1);
	assert(flow->getTarget() == s2);
	assert(flow->getId() == "f1");

	delete flow;
}

void unitTestFlow_construtor3 ()
{
	SystemImpl ss1(2, "asd"), ss2;
	SystemImpl *s1 = &ss1;
	SystemImpl *s2 = &ss2;
	FlowImpl *flow1 = new FlowImpl("f1", s1, s2);
	FlowImpl *flow2 = new FlowImpl(flow1);

	assert(flow2->getSource() == s1);
	assert(flow2->getTarget() == s2);
	assert(flow2->getId() == "f1");	

	delete flow1;
	delete flow2;
}

void unitTestFlow_connect ()
{
	FlowImpl *flow = new FlowImpl();
	SystemImpl ss1(2, "asd"), ss2;
	SystemImpl *s1 = &ss1;
	SystemImpl *s2 = &ss2;

	flow->connect(s1, 0);
	flow->connect(s2, 1);

	assert(flow->getSource() == s1);
	assert(flow->getTarget() == s2);

	delete flow;
}

void unitTestFlow_disconnect ()
{
	SystemImpl ss1(2, "asd"), ss2;
	SystemImpl *s1 = &ss1;
	SystemImpl *s2 = &ss2;
	FlowImpl *flow = new FlowImpl("f1", s1, s2);

	flow->disconnect(0);
	flow->disconnect(1);

	assert(flow->getSource() == NULL);	
	assert(flow->getTarget() == NULL);

	delete flow;
}

class myFlow : public FlowImpl
{
public:
	float execute (float source, float target)
	{
		return source * 0.1;
	}
};

void unitTestFlow_execute ()
{
	SystemImpl ss1(100, "asd"), ss2;
	SystemImpl *s1 = &ss1;
	SystemImpl *s2 = &ss2;
	myFlow *flow = new myFlow();

	flow->connect(s1, 0);
	flow->connect(s2, 1);	

	assert(fabs(flow->execute(s1->getQuantity(), s2->getQuantity()) - 10) < 0.0001);
}

void unitTestFlow_operator_attribution ()
{
	SystemImpl ss1(2, "asd"), ss2;
	SystemImpl *s1 = &ss1;
	SystemImpl *s2 = &ss2;
	FlowImpl *flow1 = new FlowImpl("f1", s1, s2);
	FlowImpl *flow2 = new FlowImpl();

	flow2 = flow1;

	assert(flow2->getSource() == s1);
	assert(flow2->getTarget() == s2);
	assert(flow2->getId() == "f1");

}

void unitTestFlow_operator_compare ()
{
	SystemImpl ss1(2, "asd"), ss2(3, "asss");
	SystemImpl *s1 = &ss1;
	SystemImpl *s2 = &ss2;
	FlowImpl *flow1 = new FlowImpl("f1", s1, s2);
	FlowImpl *flow2 = new FlowImpl("f1", s1, s2);

	assert(flow1 == flow1);

}

void unitTestFlow_global ()
{
	unitTestFlow_construtor1 ();
	unitTestFlow_construtor2 ();
	unitTestFlow_construtor3 ();
	unitTestFlow_connect ();
	unitTestFlow_disconnect ();
	unitTestFlow_execute ();
	unitTestFlow_operator_attribution ();
	unitTestFlow_operator_compare ();
}