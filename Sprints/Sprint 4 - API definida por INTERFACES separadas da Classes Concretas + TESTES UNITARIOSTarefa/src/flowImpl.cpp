#include "flowImpl.hpp"

/**
  * \return FlowImpl
  */
FlowImpl::FlowImpl () 
{
	this->source = NULL;
	this->target = NULL;
}

/**
  * \param id is the flowImpl's name
  * \param source is the SystemImpl that gonna give energy
  * \param target is the SystemImpl that gonna recive the energy
  * \return FlowImpl
  */
FlowImpl::FlowImpl (string id, SystemImpl *source, SystemImpl *target)
{
	this->source = source;
	this->target = target;
	this->id = id;
}

/**
  * \param flowImpl is a FlowImpl, and the new one gonna be a copy of it
  * \return FlowImpl
  */
FlowImpl::FlowImpl(FlowImpl *flowImpl)
{
	this->source = flowImpl->source;
	this->target = flowImpl->target;
	this->id = flowImpl->id;
}

/**
  * \return
  */
FlowImpl::~FlowImpl()
{

}

/**
  * \param id is the FlowImpl's name
  * \return void
  */
void FlowImpl::setId (string id)
{
	this->id = id;
}

/**
  * \return string
  */
string FlowImpl::getId()
{
	return this->id;
}

/**
  * \return SystemImpl*
  */
SystemImpl* FlowImpl::getSource()
{
	return this->source;
}

/**
  * \return SystemImpl*
  */
SystemImpl* FlowImpl::getTarget()
{
	return this->target;
}

/** \param s is the systemImpl that gonna be connected
  * \param p define if is a target or a source
  * \return bool
  *
  **/
bool FlowImpl::connect (SystemImpl *s, int p)
{
	if(s != NULL)
	{
		if(p <= 0) // connect the source
		{
			if(this->source == NULL)
			{
				this->source = s;
				return 0; // successfull connection
			}
			else
				return 1; // source already connected
		}

		else // connect the target
		{
			if(this->target == NULL)
			{
				this->target = s;
				return 0; // successfull connection
			}
			else
				return 1; // target already connected
		}
	}

	return -1; // systemImpl == NULL
}

/** \param p which systemImpl gonna be disconnected, target or source
  * \return bool
  *
  **/
bool FlowImpl::disconnect (int p)
{
	if(p <= 0) // disconnect the source
	{
		if(this->source != NULL)
		{
			this->source = NULL;
			return true;
		}
	}

	else if(this->target != NULL) // disconnect the target
	{
		this->target = NULL;		
		return true;
	}

	return false; // the flowImpl is already disconnected
}

/**
  * \param source is the quantity of the FlowImpl's source
  *
  * \param target is the quantity of the FlowImpl's target
  *
  * \return float
  *
  **/
float FlowImpl::execute (float source, float target)
{
	return 0;
}

/**
  * \param flowImpl is a flowImpl that gonna be copied
  * \return FlowImpl&
  */
FlowImpl& FlowImpl::operator= (const FlowImpl &flowImpl)
{
	if(this == &flowImpl)
		return *this;

	this->id = flowImpl.id;

	this->source = flowImpl.source;
	this->target = flowImpl.target;

	return *this;
}

/**
  * \param flowImpl is a flowImpl that gonna be compared with the FlowImpl that call this function
  * \return bool
  */
bool FlowImpl::operator== (const FlowImpl &flowImpl)
{
	if(this == &flowImpl)
		return true;

	if(id == flowImpl.id && target == flowImpl.target && source == flowImpl.source)
		return true;

	return false;
}