#ifndef flow_HPP
#define flow_HPP

#include <iostream>
#include <string>

#include "system.hpp"

using namespace std;

class Flow
{

public:
	/** Default Flow's constructor */
	Flow ();
	/** Flow's constructor */
	Flow (string, System*, System*);
	/** Flow's constructor of copy */
	Flow (Flow*);
	/** Flow's destructor */
	virtual ~Flow() = 0; 
	/** Set id */
	virtual void setId (string) = 0; 
	/** Get Flow's id */
	virtual string getId() = 0; 
	/** Get Flow'd source */
	virtual System* getSource() = 0; 
	/** Get Flow's target */
	virtual System* getTarget() = 0; 

	/** Connect one System to the Flow. */
	virtual bool connect (System*, int) = 0;
	/** Disconnect one Flow's system. */				 			 
	virtual bool disconnect (int) = 0; 
	/** Define the equation to the Flow */
	virtual float execute (float, float) = 0; 

protected:
	/** The system that gonna recive the quantity */
	System *target; 
	/** The system that gonna give the quantity */ 
	System *source; 
	/** Flow's Id */ 
	string id; 

private:
	/** operator = overload */
	virtual Flow& operator= (const Flow&) = 0; 
	/** operator == overload */
	virtual bool operator== (const Flow&) = 0; 
};

#endif