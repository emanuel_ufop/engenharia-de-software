#ifndef modelImpl_HPP
#define modelImpl_HPP

#include <vector>
#include <iostream>
#include <iomanip>

#include "flowImpl.hpp"

class ModelImpl
{

public:
	/** ModelImpl's constructor */
	ModelImpl(); 
	/** ModelImpl's constructor of copy*/
	ModelImpl(ModelImpl*);
	/** ModelImpl's destructor */
	virtual ~ModelImpl(); 

	/** Assings a FlowImpl at a ModelImpl */
	bool add (FlowImpl*); 
	/** Assings a SystemImpl at a ModelImpl */
	bool add (SystemImpl*); 
	/** Remove a FlowImpl at a ModelImpl */
	bool remove (FlowImpl*); 
	/** Remove a SystemImpl at a ModelImpl */
	bool remove (SystemImpl*); 
	/** Execute all flowImpl equations on their respective systemImpls. */
	void simulate (int, int); 
	/** Print the ModelImpl's systemImpls and flowImpls. */
	void show (); 
	/** Give a report of the simulation */
	void report (); 

protected:
	
	vector<FlowImpl*> flowImpl_vector; /*!< ModelImpl's flowImpls */ 
	vector<SystemImpl*> systemImpl_vector; /*!< ModelImpl's systemImpls */
	
private:
	/** operator = overload */
	ModelImpl& operator= (const ModelImpl&); 
	/** operator == overload */
	bool operator== (const ModelImpl&); 

};

#endif