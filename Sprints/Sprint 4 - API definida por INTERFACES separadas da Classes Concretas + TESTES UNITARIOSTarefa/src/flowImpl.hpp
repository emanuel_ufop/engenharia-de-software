#ifndef flowImpl_HPP
#define flowImpl_HPP

#include <iostream>
#include <string>

#include "systemImpl.hpp"

using namespace std;

class FlowImpl
{

public:
	/** Default Flow's constructor */
	FlowImpl ();
	/** Flow's constructor */
	FlowImpl (string, SystemImpl*, SystemImpl*);
	/** Flow's constructor of copy */
	FlowImpl (FlowImpl*);
	/** Flow's destructor */
	virtual ~FlowImpl(); 
	/** Set id */
	void setId (string); 
	/** Get Flow's id */
	string getId(); 
	/** Get Flow'd source */
	SystemImpl* getSource(); 
	/** Get Flow's target */
	SystemImpl* getTarget(); 

	/** Connect one SystemImpl to the Flow. */
	bool connect (SystemImpl*, int);
	/** Disconnect one Flow's systemImpl. */				 			 
	bool disconnect (int); 
	/** Define the equation to the Flow */
	virtual float execute (float, float); 

protected:
	/** The systemImpl that gonna recive the quantity */
	SystemImpl *target; 
	/** The systemImpl that gonna give the quantity */ 
	SystemImpl *source; 
	/** Flow's Id */ 
	string id; 

private:
	/** operator = overload */
	FlowImpl& operator= (const FlowImpl&); 
	/** operator == overload */
	bool operator== (const FlowImpl&); 
};

#endif