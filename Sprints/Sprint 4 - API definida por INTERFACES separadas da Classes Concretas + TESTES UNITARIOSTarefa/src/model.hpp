#ifndef model_HPP
#define model_HPP

#include <vector>
#include <iostream>
#include <iomanip>

#include "flow.hpp"

class Model
{

public:
	/** Model's constructor */
	Model();
	/** Model's constructor of copy*/
	Model(Model*);
	/** Model's destructor */
	virtual ~Model() = 0;

	/** Assings a Flow at a Model */
	virtual bool add (Flow*) = 0;
	/** Assings a System at a Model */
	virtual bool add (System*) = 0;
	/** Remove a Flow at a Model */
	virtual bool remove (Flow*) = 0;
	/** Remove a System at a Model */
	virtual bool remove (System*) = 0;
	/** Execute all flow equations on their respective systems. */
	virtual void simulate (int, int) = 0;
	/** Print the Model's systems and flows. */
	virtual void show () = 0;
	/** Give a report of the simulation */
	virtual void report () = 0;

protected:
	
	vector<Flow*> flow_vector; /*!< Model's flows */ 
	vector<System*> system_vector; /*!< Model's systems */ 

private:
	/** operator = overload */
	virtual Model& operator= (const Model&) = 0;
	/** operator == overload */
	virtual bool operator== (const Model&) = 0;

};

#endif