#ifndef system_HPP
#define system_HPP

#include <iostream>
#include <string>

using namespace std;

class System
{

public:
	/** System's default constructor */
	System (); 
	/** System's constructor */
	System (float, string);
	/** System's constructor of copy */
	System (System *System);
	/** System's destructor */
	virtual ~System() = 0; 

	/** Set System's quantity */
	virtual void setQuantity (float) = 0;
	/** Set System's id */
	virtual void setId (string) = 0; 
	/** Get System's quantity */
	virtual float getQuantity () = 0; 
	/** Get System's Id */
	virtual string getId () = 0; 

	/** Increase quantity. */
	virtual void increaseQuantity(float) = 0;
	/** Decrease quantity. */
	virtual void decreaseQuantity(float) = 0; 

protected:
	/** System's quantity*/ 
	float quantity; 
	/** System's Id*/ 
	string id; 

private:
	/** Operator overload */
	virtual System& operator= (const System&) = 0; 
	/**  Operator == overload */
	virtual bool operator== (const System&) = 0; 
};

#endif