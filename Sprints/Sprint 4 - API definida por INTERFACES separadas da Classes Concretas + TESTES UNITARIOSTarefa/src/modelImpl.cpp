#include "modelImpl.hpp"

using namespace std;

/**
  * \return ModelImpl
  */
ModelImpl::ModelImpl()
{
	this->flowImpl_vector.clear();
	this->systemImpl_vector.clear();
}

/**
  * \param ModelImpl is a ModelImpl, and the new one gonna be a copy of it
  * \return ModelImpl
  */
ModelImpl::ModelImpl(ModelImpl *modelImpl)
{
	flowImpl_vector = modelImpl->flowImpl_vector;
	systemImpl_vector = modelImpl->systemImpl_vector;
}

/**
  * \return Sem retorno
  */
ModelImpl::~ModelImpl ()
{

}

/** \param flowImpl is the flowImpl that gonna be added
  * \return bool
  **/
bool ModelImpl::add (FlowImpl *flowImpl)
{
	this->flowImpl_vector.push_back(flowImpl);
	return true;
}

/** \param systemImpl is the systemImpl that gonna be added
  * \return bool
  **/
bool ModelImpl::add (SystemImpl *systemImpl)
{
	this->systemImpl_vector.push_back(systemImpl);
	return true;
}

/** \param flowImpl is the flowImpl that gonna be removed
  * \return bool
  **/
bool ModelImpl::remove (FlowImpl *flowImpl)
{
	int size = flowImpl_vector.size();

	for(int i = 0; i < size; i++)
	{
		if(this->flowImpl_vector[i]->getId() == flowImpl->getId())
		{
			this->flowImpl_vector.erase(this->flowImpl_vector.begin() + i);
			return true;
		}
	}

	return false; // the flowImpl didn't exist
}

/** \param systemImpl is the systemImpl that gonna be removed
  * \return bool
  **/
bool ModelImpl::remove (SystemImpl *systemImpl)
{
	int size = systemImpl_vector.size();
	 
	for(int i = 0; i < size; i++)
	{
		if(systemImpl_vector[i]->getId() == systemImpl->getId())
		{
			systemImpl_vector.erase(systemImpl_vector.begin() + i);
			return true;
		}
	}

	return false; // the systemImpl didn't exist 
}

/** \param begin is the begin of the loop
  * \param end is the end of the loop
  * \return void
  *
  **/
void ModelImpl::simulate (int begin, int end)
{
	int size = flowImpl_vector.size(); // get a vector size
	float *v = new float[size]; // alocate a float vector

	for(int i = begin; i < end; i++)
	{
		for(int j = 0; j < size; j++)
			v[j] = flowImpl_vector[j]->execute(flowImpl_vector[j]->getSource()->getQuantity(), flowImpl_vector[j]->getTarget()->getQuantity()); // save the results of executes

		for(int k = 0; k < size; k++)
		{
			// set the new systens' value
			if(flowImpl_vector[k]->getTarget() != NULL)
				flowImpl_vector[k]->getTarget()->increaseQuantity(v[k]);

			if(flowImpl_vector[k]->getSource() != NULL)
				flowImpl_vector[k]->getSource()->decreaseQuantity(v[k]);		
		}
	}

}

/**
  * \return void
  **/
void ModelImpl::show ()
{
	for(vector<SystemImpl*>::iterator it = this->systemImpl_vector.begin(); it != systemImpl_vector.end(); it++)
	{
		cout << "SystemImpl: " << (*it)->getId() << "   Quantity: " << (*it)->getQuantity() << endl;
	}

	for(vector<FlowImpl*>::iterator it = this->flowImpl_vector.begin(); it != flowImpl_vector.end(); it++)
	{
		cout << "FlowImpl " << (*it)->getId() << " -> Source: ";
		if((*it)->getSource() != NULL)
			cout << (*it)->getSource()->getId();
		else
			cout << fixed << setw(10) << "NULL";

		cout << "   Target: ";
		if((*it)->getTarget() != NULL)
			cout << (*it)->getTarget()->getId();
		else
			cout << "NULL";

		cout << endl;
	}
}

/**
  * \return void
  *
  **/
void report ()
{

}

/**
  * \param ModelImpl is a ModelImpl that gonna be copied
  * \return ModelImpl&
  */
ModelImpl& ModelImpl::operator= (const ModelImpl &modelImpl)
{
	if(this == &modelImpl)
		return *this;


	for(vector<FlowImpl*>::iterator it = this->flowImpl_vector.begin(); it != this->flowImpl_vector.end(); it++)
		delete *it;

	this->flowImpl_vector.clear();

	for(vector<SystemImpl*>::iterator it = this->systemImpl_vector.begin(); it != this->systemImpl_vector.end(); it++)
		delete *it;

	this->systemImpl_vector.clear();


	for(int i = 0, size = modelImpl.flowImpl_vector.size(); i < size; i++)
	{
		FlowImpl *aux = new FlowImpl;
		aux = modelImpl.flowImpl_vector[i];
		this->flowImpl_vector.push_back(aux);
	}

	for(int i = 0, size = modelImpl.systemImpl_vector.size(); i < size; i++)
	{
		SystemImpl *aux = new SystemImpl;
		aux = modelImpl.systemImpl_vector[i];
		this->systemImpl_vector.push_back(aux);
	}

	/*for(vector<FlowImpl>::iterator it = ModelImpl.flowImpl_vector.begin(); it != ModelImpl.flowImpl_vector.end(); it++)
		this->flowImpl_vector.push_back(*it);

	for(vector<SystemImpl>::iterator it = ModelImpl.systemImpl_vector.begin(); it != ModelImpl.systemImpl_vector.end(); it++)
		this->systemImpl_vector.push_back(*it);*/

	return *this;
}

/**
  * \param ModelImpl is a ModelImpl that gonna be compared with the ModelImpl that call this function
  * \return bool
  */
bool ModelImpl::operator== (const ModelImpl &modelImpl)
{
	if(this == &modelImpl) 
		return true;

	if(flowImpl_vector.size() != modelImpl.flowImpl_vector.size() || systemImpl_vector.size() != modelImpl.systemImpl_vector.size())
		return false;

	if(flowImpl_vector == modelImpl.flowImpl_vector && systemImpl_vector == modelImpl.systemImpl_vector)
		return true;

	return false;
}