var searchData=
[
  ['refcount_55',['refCount',['../class_body.html#a59ae961812625b8636071ba61b1a75fc',1,'Body']]],
  ['refcount_5f_56',['refCount_',['../class_body.html#a70b46681762de310d6a3fba89ba2d721',1,'Body']]],
  ['remove_57',['remove',['../class_model.html#a9bc6d369f92ba6368227e2d3a9838e4a',1,'Model::remove(Flow *)=0'],['../class_model.html#a9f4cb42b7c0cb3ba5cb0118bc8815de5',1,'Model::remove(System *)=0'],['../class_model_body.html#a3c6588cd67336b57e75e9d0044ad040a',1,'ModelBody::remove(Flow *)'],['../class_model_body.html#adb5db54723ff39e0f5409930cd502a72',1,'ModelBody::remove(System *)'],['../class_model_handle.html#ae8847c01af5fa7f4df6d84bfb58843a1',1,'ModelHandle::remove(Flow *f)'],['../class_model_handle.html#a8bf87924b9eaaa0827db33e6a6e28675',1,'ModelHandle::remove(System *s)']]],
  ['report_58',['report',['../class_model.html#af679e89214a7cd7c8259af770c0731c9',1,'Model::report()'],['../class_model_body.html#a489ea2adfa79758d38314595237d42bb',1,'ModelBody::report()'],['../class_model_handle.html#a8bbd0eced8a83e474966eaf374d9c372',1,'ModelHandle::report()']]]
];
