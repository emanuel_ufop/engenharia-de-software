#ifndef functionalTest1_HPP
#define functionalTest1_HPP

#include <math.h>
#include <assert.h>

#include "../../src/model.hpp"
#include "../../src/flowImpl.hpp"

using namespace std;

class myFlow1 : public FlowHandle
{
	/** Define the equation to the Flow */
	float execute (float, float);
};

/** This is a functional test*/
void test1 ();

#endif