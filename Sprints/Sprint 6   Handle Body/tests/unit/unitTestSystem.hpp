#ifndef unitTestSystem_HPP
#define unitTestSystem_HPP

#include <assert.h>
#include <math.h>

#include "../../src/systemImpl.hpp"
#include "../../src/system.hpp"

void unitTestSystem_constructorSystem1 ();
void unitTestSystem_constructorSystem2 ();
void unitTestSystem_constructorSystem3 ();
void unitTestSystem_destructorSystem ();
void unitTestSystem_increaseQuantity ();
void unitTestSystem_decreaseQuantity ();
void unitTestSystem_operator_attribution ();
void unitTestSystem_operator_compare ();
void unitTestSystem_global ();

#endif