#include "unitTestSystem.hpp"

void unitTestSystem_constructorSystem1 ()
{
	SystemHandle *system = new SystemHandle();

	assert(fabs(system->getQuantity() - 0) < 0.0001);
	assert(system->getId() == "");

}

void unitTestSystem_constructorSystem2 ()
{
	SystemHandle *system = new SystemHandle(1.234, "mySystem");

	assert(fabs(system->getQuantity() - 1.234) < 0.0001);
	assert(system->getId() == "mySystem");

}

void unitTestSystem_increaseQuantity ()
{
	SystemHandle *system = new SystemHandle();

	system->increaseQuantity(1.01);

	assert(fabs(system->getQuantity() - 1.01) < 0.0001);

}

void unitTestSystem_decreaseQuantity ()
{
	SystemHandle *system = new SystemHandle(1.234, "mySystem");

	system->decreaseQuantity(1.234);

	assert(fabs(system->getQuantity() - 0) < 0.0001);

}

void unitTestSystem_global ()
{
	unitTestSystem_constructorSystem1 ();
	unitTestSystem_constructorSystem2 ();
	unitTestSystem_increaseQuantity ();
	unitTestSystem_decreaseQuantity ();
}