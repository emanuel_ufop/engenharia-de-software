#ifndef unitTestModel_HPP
#define unitTestModel_HPP

#include <assert.h>
#include <math.h>
#include <iostream>

#include "../../src/model.hpp"
#include "../../src/flowImpl.hpp"

void unitTestModel_constructor ();
void unitTestModel_add_flow (); 
void unitTestModel_add_system (); 
void unitTestModel_remove_flow (); 
void unitTestModel_remove_system (); 
void unitTestModel_simulate (); 
void unitTestModel_operator_attribution ();
void unitTestModel_operator_compare ();
void unitTestModel_global ();

#endif