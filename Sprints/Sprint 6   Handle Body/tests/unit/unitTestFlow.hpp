#ifndef unitTestFlow_HPP
#define unitTestFlow_HPP

#include <assert.h>
#include <math.h>
#include <iostream>

#include "../../src/flowImpl.hpp"
#include "../../src/flow.hpp"
#include "../../src/system.hpp"
#include "../../src/systemImpl.hpp"

void unitTestFlow_construtor1 ();
void unitTestFlow_construtor2 ();
void unitTestFlow_construtor3 ();
void unitTestFlow_connect ();
void unitTestFlow_disconnect ();
void unitTestFlow_execute ();
void unitTestFlow_getSource (); 
void unitTestFlow_getTarget (); 
void unitTestFlow_operator_attribution ();
void unitTestFlow_operator_compare ();
void unitTestFlow_global ();

#endif