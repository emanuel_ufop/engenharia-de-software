#include "unitTestFlow.hpp"

void unitTestFlow_construtor1 ()
{
	FlowHandle *flow = new FlowHandle();

	assert(flow->getTarget() == NULL);
	assert(flow->getSource() == NULL);
}

void unitTestFlow_construtor2 ()
{
	SystemHandle *s1 = new SystemHandle(2, "asd");
	SystemHandle *s2 = new SystemHandle();
	FlowHandle *flow = new FlowHandle("f1", s1, s2);

	cout << "id: "<< flow->getSource()->getId() << endl;

	assert(flow->getSource()->getId() == s1->getId());
	assert(flow->getTarget()->getId() == s2->getId());
	assert(flow->getId() == "f1");
}

void unitTestFlow_connect ()
{
	FlowHandle *flow = new FlowHandle();
	SystemHandle *s1 = new SystemHandle(2, "asd");
	SystemHandle *s2 = new SystemHandle();

	flow->connect(s1, 0);
	flow->connect(s2, 1);

	assert(flow->getSource()->getId() == s1->getId());
	assert(flow->getTarget()->getId() == s2->getId());
}

void unitTestFlow_disconnect ()
{
	SystemHandle *s1 = new SystemHandle(2, "asd");
	SystemHandle *s2 = new SystemHandle();
	FlowHandle *flow = new FlowHandle("f1", s1, s2);

	flow->disconnect(0);
	flow->disconnect(1);

	assert(flow->getSource() == NULL);	
	assert(flow->getTarget() == NULL);
}


void unitTestFlow_global ()
{
	unitTestFlow_construtor1 ();
	// unitTestFlow_construtor2 ();
	unitTestFlow_connect ();
	unitTestFlow_disconnect ();
}