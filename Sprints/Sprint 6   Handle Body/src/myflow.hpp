#ifndef MYFLOW_HPP
#define MYFLOW_HPP

class myFlow : public Flow
{
	~myFlow();
	
	float execute (float, float); /*!< A virtual function to set the Flow's equation*/
};

#endif