#include "SystemImpl.hpp"

using namespace std;

/**
  * \return SystemBody
  */
SystemBody::SystemBody ()
{
	this->quantity = 0;
	this->id[0] = '\0';
}

/**
  * \param quantuty is a quantity or energy to save
  * \param id is the id of the SystemBody
  * \return SystemBody
  */
SystemBody::SystemBody (float quantity, string id)
{
	this->quantity = quantity;
	this->id = id;
}

/**
  * \param SystemBody is a SystemBody, and the new one gonna be a copy of it
  * \return SystemBody
  */
SystemBody::SystemBody (SystemBody *SystemBody)
{
	this->quantity = SystemBody->quantity;
	this->id = SystemBody->id;
}

/**
  * \return
  */
SystemBody::~SystemBody ()
{

}

/**
  * \param quantity is the new value of quantity
  * \return void
  */
void SystemBody::setQuantity (float quantity)
{
	this->quantity = quantity;
}

/**
  * \param id is the new name of the SystemBody
  * \return void
  */
void SystemBody::setId (string id)
{
	this->id = id;
}

/**
  * \return float
  */
float SystemBody::getQuantity ()
{
	return this->quantity;
}

/**
  * \return string
  */
string SystemBody::getId ()
{
	return this->id;
}

/** \param value is the quantity that gonna be add to the SystemBody's quantity
  * \return void
  *
  **/
void SystemBody::increaseQuantity (float value)
{
	this->quantity += value;
}

/** \param value is the quantity that gonna be remove to the SystemBody's quantity
  * \return void
  *
  **/
void SystemBody::decreaseQuantity (float value)
{
	this->quantity -= value;
}

/**
  * \param SystemBody is the SystemBody that gonna be copied
  * \return SystemBody&
  */
SystemBody& SystemBody::operator= (const SystemBody &SystemBody)
{
	if(this == &SystemBody)
		return *this;

	this->id = SystemBody.id;
	this->quantity = SystemBody.quantity;

	return *this;
}

/**
  * \param SystemBody is the SystemBody that gonna be compared with the SystemBody that call this function
  * \return bool
  */
bool SystemBody::operator== (const SystemBody &SystemBody)
{
  if(this == &SystemBody)
    return true; 
  
	if(this->id == SystemBody.id && this->quantity == SystemBody.quantity)
		return true;

	return false;
}