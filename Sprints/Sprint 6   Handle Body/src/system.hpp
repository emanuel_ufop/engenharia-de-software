#ifndef system_HPP
#define system_HPP

#include <iostream>
#include <string>

using namespace std;

class System
{

public:
	/** Set System's quantity */
	virtual void setQuantity (float) = 0;
	/** Set System's id */
	virtual void setId (string) = 0; 
	/** Get System's quantity */
	virtual float getQuantity () = 0; 
	/** Get System's Id */
	virtual string getId () = 0; 

	/** Increase quantity. */
	virtual void increaseQuantity(float) = 0;
	/** Decrease quantity. */
	virtual void decreaseQuantity(float) = 0; 

};

#endif