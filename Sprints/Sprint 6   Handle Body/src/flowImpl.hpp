#ifndef flowImphl_HPP
#define flowImphl_HPP

#include "system.hpp"
#include "flow.hpp"
#include "handle_body.h"

using namespace std;

class FlowBody : public Body
{

public:
	/** Default Flow's constructor */
	FlowBody ();
	/** Flow's constructor */
	FlowBody (string, System*, System*);
	/** Flow's constructor of copy */
	FlowBody (FlowBody*);
	/** Flow's destructor */
	virtual ~FlowBody(); 
	/** Set id */
	void setId (string); 
	/** Get Flow's id */
	string getId(); 
	/** Get Flow'd source */
	System* getSource(); 
	/** Get Flow's target */
	System* getTarget(); 

	/** Connect one SystemImpl to the Flow. */
	bool connect (System*, int);
	/** Disconnect one Flow's systemImpl. */				 			 
	bool disconnect (int); 
	/** Define the equation to the Flow */
	virtual float execute (float, float); 

protected:
	/** The systemImpl that gonna recive the quantity */
	System *target; 
	/** The systemImpl that gonna give the quantity */ 
	System *source; 
	/** Flow's Id */ 
	string id; 

private:
	/** operator = overload */
	FlowBody& operator= (const FlowBody&); 
	/** operator == overload */
	bool operator== (const FlowBody&); 
};

class FlowHandle : public Flow, public Handle<FlowBody>
{
public:
	/** Default Flow's constructor */
	FlowHandle ()
	{
		pImpl_ = new FlowBody();
	}
	/** Flow's constructor */
	FlowHandle (string id, System* s, System* t)
	{
		pImpl_ = new FlowBody(id, s, t);
	}
	/** Flow's destructor */
	virtual ~FlowHandle()
	{
		delete pImpl_;
	}
	/** Set id */
	void setId (string id)
	{
		pImpl_->setId(id);
	}
	/** Get Flow's id */
	string getId()
	{
		return pImpl_->getId();
	} 
	/** Get Flow'd source */
	System* getSource()
	{
		return pImpl_->getSource();
	}
	/** Get Flow's target */
	System* getTarget()
	{
		return pImpl_->getTarget();
	}

	/** Connect one SystemImpl to the Flow. */
	bool connect (System* s, int p)
	{
		return pImpl_->connect(s, p);
	}
	/** Disconnect one Flow's systemImpl. */				 			 
	bool disconnect (int p) 
	{
		return pImpl_->disconnect(p);
	}
	/** Define the equation to the Flow */
	virtual float execute (float s, float t)
	{
		return pImpl_->execute(s, t);
	}
};

#endif