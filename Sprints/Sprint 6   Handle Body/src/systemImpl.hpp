#ifndef systemImpl_HPP
#define systemImpl_HPP

#include <iostream>
#include <string>

#include "system.hpp"
#include "handle_body.h"

using namespace std;

class SystemBody : public Body
{

public:
	/** SystemBody's default constructor */
	SystemBody (); 
	/** SystemBody's constructor */
	SystemBody (float, string);
	/** SystemBody's constructor of copy */
	SystemBody (SystemBody *SystemBody);
	/** SystemBody's destructor */
	virtual ~SystemBody(); 

	/** Set SystemBody's quantity */
	void setQuantity (float);
	/** Set SystemBody's id */
	void setId (string); 
	/** Get SystemBody's quantity */
	float getQuantity (); 
	/** Get SystemBody's Id */
	string getId (); 

	/** Increase quantity. */
	void increaseQuantity(float); 
	/** Decrease quantity. */
	void decreaseQuantity(float); 
	/** Operator overload */
	SystemBody& operator= (const SystemBody&); 
	/**  Operator == overload */
	bool operator== (const SystemBody&); 

protected:
	/** SystemBody's quantity*/ 
	float quantity; 
	/** SystemBody's Id*/ 
	string id; 

};

class SystemHandle : public System, public Handle<SystemBody>
{

public:
	/** SystemBody's default constructor */
	SystemHandle ()
	{
		pImpl_ = new SystemBody();
	}
	/** SystemBody's constructor */
	SystemHandle (float quantity, string id)
	{
		pImpl_ = new SystemBody(quantity, id);
	}
	/** SystemBody's destructor */
	virtual ~SystemHandle()
	{
		delete pImpl_;
	}

	/** Set SystemBody's quantity */
	void setQuantity (float value)
	{
		pImpl_->setQuantity(value);
	}
	/** Set SystemBody's id */
	void setId (string id)
	{
		pImpl_->setId(id);
	} 
	/** Get SystemBody's quantity */
	float getQuantity ()
	{
		return pImpl_->getQuantity();
	}
	/** Get SystemBody's Id */
	string getId ()
	{
		return pImpl_->getId();
	}

	/** Increase quantity. */
	void increaseQuantity(float value)
	{
		pImpl_->increaseQuantity(value);
	}
	/** Decrease quantity. */
	void decreaseQuantity(float value)
	{
		pImpl_->decreaseQuantity(value);
	}
};

#endif