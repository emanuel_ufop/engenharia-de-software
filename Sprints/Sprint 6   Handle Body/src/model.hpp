#ifndef model_HPP
#define model_HPP

#include "flow.hpp"

class Model
{
	/** Assings a FlowImpl at a ModelImpl */
	virtual bool add (Flow*) = 0;
	/** Assings a SystemImpl at a ModelImpl */
	virtual bool add (System*) = 0;

public:
	/** Tell if a there is a system or not in the model */
	virtual bool searchSystem (string) = 0;
	/** Tell if a there is a flow or not in the model */
	virtual bool searchFlow (string) = 0;
	/** Return a system*/
	virtual System* findSystem (string) = 0;
	/** Return a flow*/
	virtual Flow* findFlow (string) = 0;
	/** Create and add a new System  */
	virtual System* createSystem () = 0;
	/** Create and add a new System  */
	virtual System* createSystem (string, float) = 0;
	/** Create and add a new Flow */
    template <typename TYPE_FLOW>
    Flow* createFlow (string id, System *s, System *t)
    {
    	Flow *f = new TYPE_FLOW();
    	f->setId(id);
    	f->connect(s, 0);
    	f->connect(t, 1);
    	add(f);
    	return f;
    }

    template <typename TYPE_FLOW>
    Flow* createFlow (string id)
    {
    	Flow *f = new TYPE_FLOW();
    	f->setId(id);
    	add(f);
    	return f;
    }

	/** Remove a Flow at a Model */
	virtual bool remove (Flow*) = 0;
	/** Remove a System at a Model */
	virtual bool remove (System*) = 0;
	/** Connect a flows and systems */
	virtual bool connect(string, string, int) = 0;
	/** Execute all flow equations on their respective systems. */
	virtual void simulate (int, int) = 0;
	/** Print the Model's systems and flows. */
	virtual void show () = 0;
	/** Give a report of the simulation */
	virtual void report () = 0;
	/** Creat a new model */
	static Model* createModel();

};

#endif