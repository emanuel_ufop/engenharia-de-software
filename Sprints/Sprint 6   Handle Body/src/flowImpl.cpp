#include "flowImpl.hpp"

/**
  * \return FlowBody
  */
FlowBody::FlowBody () 
{
	source = NULL;
	target = NULL;
	id = "";
}

/**
  * \param id is the FlowBody's name
  * \param source is the System that gonna give energy
  * \param target is the System that gonna recive the energy
  * \return FlowBody
  */
FlowBody::FlowBody (string id, System *source, System *target)
{
	source = source;
	target = target;
	id = id;
}

/**
  * \param FlowBody is a FlowBody, and the new one gonna be a copy of it
  * \return FlowBody
  */
FlowBody::FlowBody(FlowBody *FlowBody)
{
	source = FlowBody->source;
	target = FlowBody->target;
	id = FlowBody->id;
}

/**
  * \return
  */
FlowBody::~FlowBody()
{

}

/**
  * \param id is the FlowBody's name
  * \return void
  */
void FlowBody::setId (string id)
{
	this->id = id;
}

/**
  * \return string
  */
string FlowBody::getId()
{
	return id;
}

/**
  * \return System*
  */
System* FlowBody::getSource()
{
	return source;
}

/**
  * \return System*
  */
System* FlowBody::getTarget()
{
	return target;
}

/** \param s is the System that gonna be connected
  * \param p define if is a target or a source
  * \return bool
  *
  **/
bool FlowBody::connect (System *s, int p)
{
	if(s != NULL)
	{
		if(p <= 0) // connect the source
		{
			if(source == NULL)
			{
				source = s;
				return 0; // successfull connection
			}
			else
				return 1; // source already connected
		}

		else // connect the target
		{
			if(target == NULL)
			{
				target = s;
				return 0; // successfull connection
			}
			else
				return 1; // target already connected
		}
	}

	return -1; // System == NULL
}

/** \param p which System gonna be disconnected, target or source
  * \return bool
  *
  **/
bool FlowBody::disconnect (int p)
{
	if(p <= 0) // disconnect the source
	{
		if(this->source != NULL)
		{
			source = NULL;
			return true;
		}
	}

	else if(this->target != NULL) // disconnect the target
	{
		target = NULL;		
		return true;
	}

	return false; // the FlowBody is already disconnected
}

/**
  * \param source is the quantity of the FlowBody's source
  *
  * \param target is the quantity of the FlowBody's target
  *
  * \return float
  *
  **/
float FlowBody::execute (float source, float target)
{
	return 0;
}

/**
  * \param FlowBody is a FlowBody that gonna be copied
  * \return FlowBody&
  */
FlowBody& FlowBody::operator= (const FlowBody &FlowBody)
{
	if(this == &FlowBody)
		return *this;

	id = FlowBody.id;

	source = FlowBody.source;
	target = FlowBody.target;

	return *this;
}

/**
  * \param FlowBody is a FlowBody that gonna be compared with the FlowBody that call this function
  * \return bool
  */
bool FlowBody::operator== (const FlowBody &FlowBody)
{
	if(this == &FlowBody)
		return true;

	if(id == FlowBody.id && target == FlowBody.target && source == FlowBody.source)
		return true;

	return false;
}