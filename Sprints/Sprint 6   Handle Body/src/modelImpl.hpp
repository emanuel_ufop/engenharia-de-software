#ifndef modelImpl_HPP
#define modelImpl_HPP

#include <vector>
#include <iomanip>
#include <iostream>

#include "handle_body.h"
#include "flowImpl.hpp"
#include "SystemImpl.hpp"
#include "model.hpp"


class ModelBody : public Body
{

public:
	/** ModelBody's constructor */
	ModelBody(); 
	/** ModelBody's constructor of copy*/
	ModelBody(ModelBody*);
	/** ModelBody's destructor */
	virtual ~ModelBody(); 

	/** Tell if a there is a system or not in the model */
	bool searchSystem(string);
	/** Tell if a there is a flow or not in the model */
	bool searchFlow (string id);
	/** Return a dystem */
	System* findSystem(string);
	/** Return a flow*/
	Flow* findFlow(string);
	/** Create and add a new System  */
	System* createSystem();
	/** Create and add a new System  */
	System* createSystem(string, float);
	/** Remove a FlowImpl at a ModelBody */
	bool remove (Flow*); 
	/** Remove a SystemImpl at a ModelBody */
	bool remove (System*); 
	/** Connect a flow and a system */
	bool connect(string, string, int);
	/** Execute all flowImpl equations on their respective systemImpls. */
	void simulate (int, int); 
	/** Print the ModelBody's systemImpls and flowImpls. */
	void show (); 
	/** Give a report of the simulation */
	void report (); 
	/** Create a new Model */
	static Model* createModel();
	/** Assings a FlowImpl at a ModelBody */
	bool add (Flow*); 
	/** Assings a SystemImpl at a ModelBody */
	bool add (System*); 
	/** operator = overload */
	ModelBody& operator= (const ModelBody&); 
	/** operator == overload */
	bool operator== (const ModelBody&); 

protected:
	
	vector<Flow*> flow_vector; /*!< ModelBody's flowImpls */ 
	vector<System*> system_vector; /*!< ModelBody's systemImpls */
	
	

};

class ModelHandle : public Model, public Handle<ModelBody>
{
public:
	/** ModelBody's constructor */
	ModelHandle()
	{
		pImpl_ = new ModelBody();
	}
	/** ModelBody's constructor of copy*/
	virtual ~ModelHandle()
	{
		delete pImpl_;
		delete this;
	}

	/** Tell if a there is a system or not in the model */
	bool searchSystem(string id)
	{
		return pImpl_->searchSystem(id);
	}
	/** Tell if a there is a flow or not in the model */
	bool searchFlow (string id)
	{
		return pImpl_->searchFlow(id);
	}
	/** Return a dystem */
	System* findSystem(string id)
	{
		return pImpl_->findSystem(id);
	}
	/** Return a flow*/
	Flow* findFlow(string id)
	{
		return pImpl_->findFlow(id);
	}
	/** Create and add a new System  */
	System* createSystem()
	{
		return pImpl_->createSystem();
	}
	/** Create and add a new System  */
	System* createSystem(string id, float quantity)
	{
		return pImpl_->createSystem(id, quantity);
	}
	/** Remove a FlowImpl at a ModelBody */
	bool remove (Flow* f)
	{
		return pImpl_->remove(f);
	} 
	/** Remove a SystemImpl at a ModelBody */
	bool remove (System* s)
	{
		return pImpl_->remove(s);
	} 
	/** Connect a flow and a system */
	bool connect(string f , string s, int p)
	{	
		return pImpl_->connect(f, s, p);
	}
	/** Execute all flowImpl equations on their respective systemImpls. */
	void simulate (int start, int end)
	{
		pImpl_->simulate(start, end);
	} 
	/** Print the ModelBody's systemImpls and flowImpls. */
	void show ()
	{
		pImpl_->show();
	} 
	/** Give a report of the simulation */
	void report ()
	{
		pImpl_->report();
	} 
	/** Create a new Model */
	static Model* createModel();
	
private:
	/** Assings a FlowImpl at a ModelBody */
	bool add (Flow* f)
	{
		return pImpl_->add(f);
	}
	/** Assings a SystemImpl at a ModelBody */
	bool add (System* s)
	{
		return pImpl_->add(s);
	}
};

#endif