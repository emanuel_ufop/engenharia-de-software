#include "modelImpl.hpp"

using namespace std;

/**
  * \return ModelBody
  */
ModelBody::ModelBody()
{
	flow_vector.clear();
	system_vector.clear();
}

/**
  * \param ModelBody is a ModelBody, and the new one gonna be a copy of it
  * \return ModelBody
  */
ModelBody::ModelBody(ModelBody *ModelBody)
{
	flow_vector.clear();
	system_vector.clear();

	flow_vector = ModelBody->flow_vector;
	system_vector = ModelBody->system_vector;
}

/**
  * \return Sem retorno
  */
ModelBody::~ModelBody ()
{
	for(vector<System*>::iterator it = this->system_vector.begin(); it != system_vector.end(); it++)
		delete (SystemHandle*)*it;
	system_vector.clear();

	for(vector<Flow*>::iterator it = this->flow_vector.begin(); it != flow_vector.end(); it++)
		delete (FlowHandle*)*it;
	flow_vector.clear();

	delete this;
}

/**
  * \param id is the parameter used to find the system
  * \return bool
  */
bool ModelBody::searchSystem (string id)
{
	for(vector<System*>::iterator it = this->system_vector.begin(); it != system_vector.end(); it++)
		if((*it)->getId() == id)
			return true;

	return false;
}

/**
  * \param id is the parameter used to find the system
  * \return bool
  */
bool ModelBody::searchFlow (string id)
{
	for(vector<Flow*>::iterator it = this->flow_vector.begin(); it != flow_vector.end(); it++)
		if((*it)->getId() == id)
			return true;

	return false;
}

/**
  * \param id is the System's id
  * \return System
  */
System* ModelBody::findSystem(string id)
{
	for(vector<System*>::iterator it = this->system_vector.begin(); it != system_vector.end(); it++)
		if((*it)->getId() == id)
			return *it;

	return nullptr;
}

/**
  * \param id is the Flow's id
  * \return Flow
  */
Flow* ModelBody::findFlow(string id)
{
	for(vector<Flow*>::iterator it = this->flow_vector.begin(); it != flow_vector.end(); it++)
		if((*it)->getId() == id)
			return *it;

	return nullptr;
}

/** Create and add a new System
  * \return bool
  */
System* ModelBody::createSystem ()
{
	System *s = new SystemHandle();

	add(s);
	return s;
}

/** Create and add a new System
  * \param id is the new system's id
  * \param quantity is the new system's quantity
  *
  * \return bool
  */
System* ModelBody::createSystem (string id, float quantity)
{
	System *s = new SystemHandle(quantity, id);

	add(s);
	return s;
}

/** \param flow is the new flow that gonna be added
  * \return bool 
  */

/** \param FlowHandle is the FlowHandle that gonna be added
  * \return bool
  **/
bool ModelBody::add (Flow *flow)
{
	flow_vector.push_back(flow);
	return true;
}

/** \param SystemHandle is the SystemHandle that gonna be added
  * \return bool
  **/
bool ModelBody::add (System *system)
{
	
	system_vector.push_back(system);
	return true;
}

/** \param FlowHandle is the FlowHandle that gonna be removed
  * \return bool
  **/
bool ModelBody::remove (Flow *flow)
{
	int size = flow_vector.size();

	for(int i = 0; i < size; i++)
	{
		if(flow_vector[i]->getId() == flow->getId())
		{
			Flow *f = flow_vector[i];
			flow_vector.erase(flow_vector.begin() + i);
			delete (FlowHandle*)f;
			return true;
		}
	}

	return false; // the FlowHandle didn't exist
}

/** \param SystemHandle is the SystemHandle that gonna be removed
  * \return bool
  **/
bool ModelBody::remove (System *system)
{
	int size = system_vector.size();
	 
	for(int i = 0; i < size; i++)
	{
		if(system_vector[i]->getId() == system->getId())
		{
			System *s = system_vector[i];
			system_vector.erase(system_vector.begin() + i);
			delete (SystemHandle*)s;
			return true;
		}
	}

	return false; // the system didn't exist 
}

/**	\param id Flow is the flow's id
  *	\param id System is the system's id
  *	\param p is to define which system gonna be connected, target or source
  * \return true
  */
bool ModelBody::connect(string idFlow, string idSystem, int p)
{
	bool achou = false;
	System *s = 0;

	for(vector<System*>::iterator it = this->system_vector.begin(); it != system_vector.end(); it++)
		if((*it)->getId() == idSystem)
		{
			s = *it;
			achou = true;
		}

	if(!achou)
		return false;

	for(vector<Flow*>::iterator it = this->flow_vector.begin(); it != flow_vector.end(); it++)
		if((*it)->getId() == idFlow)
			return (*it)->connect(s, p);

	return false;
}

/** \param begin is the begin of the loop
  * \param end is the end of the loop
  * \return void
  *
  **/
void ModelBody::simulate (int begin, int end)
{
	int size = flow_vector.size(); // get a vector size
	float *v = new float[size]; // alocate a float vector

	for(int i = begin; i < end; i++)
	{
		for(int j = 0; j < size; j++)
			v[j] = flow_vector[j]->execute(flow_vector[j]->getSource()->getQuantity(), flow_vector[j]->getTarget()->getQuantity()); // save the results of executes

		for(int k = 0; k < size; k++)
		{
			// set the new systens' value
			if(flow_vector[k]->getTarget() != NULL)
				flow_vector[k]->getTarget()->increaseQuantity(v[k]);

			if(flow_vector[k]->getSource() != NULL)
				flow_vector[k]->getSource()->decreaseQuantity(v[k]);		
		}
	}

}

/**
  * \return void
  **/
void ModelBody::show ()
{
	for(vector<System*>::iterator it = this->system_vector.begin(); it != system_vector.end(); it++)
	{
		cout << "System: " << (*it)->getId() << "   Quantity: " << (*it)->getQuantity() << endl;
	}

	for(vector<Flow*>::iterator it = this->flow_vector.begin(); it != flow_vector.end(); it++)
	{
		cout << "Flow " << (*it)->getId() << " -> Source: ";
		if((*it)->getSource() != NULL)
			cout << (*it)->getSource()->getId();
		else
			cout << fixed << setw(10) << "NULL";

		cout << "   Target: ";
		if((*it)->getTarget() != NULL)
			cout << (*it)->getTarget()->getId();
		else
			cout << "NULL";

		cout << endl;
	}
}

/**
  * \return void
  *
  **/
void ModelBody::report ()
{

}

/** \return Model*
  *
  */
Model* ModelHandle::createModel ()
{
    Model *m = new ModelHandle;
    return m;
}

/** \return Model*
  *
  */
Model* Model::createModel () 
{
    return ModelHandle::createModel();
}

/**
  * \param ModelBody is a ModelBody that gonna be copied
  * \return ModelBody&
  */
ModelBody& ModelBody::operator= (const ModelBody &ModelBody)
{
	if(this == &ModelBody)
		return *this;


	for(vector<Flow*>::iterator it = this->flow_vector.begin(); it != this->flow_vector.end(); it++)
		delete (FlowHandle*)*it;

	this->flow_vector.clear();

	for(vector<System*>::iterator it = this->system_vector.begin(); it != this->system_vector.end(); it++)
		delete (SystemHandle*)*it;

	this->system_vector.clear();

	this->flow_vector = ModelBody.flow_vector;

	this->system_vector = ModelBody.system_vector;

	return *this;
}

/**
  * \param ModelBody is a ModelBody that gonna be compared with the ModelBody that call this function
  * \return bool
  */
bool ModelBody::operator== (const ModelBody &ModelBody)
{
	if(this == &ModelBody) 
		return true;

	if(flow_vector.size() != ModelBody.flow_vector.size() || system_vector.size() != ModelBody.system_vector.size())
		return false;

	if(flow_vector == ModelBody.flow_vector && system_vector == ModelBody.system_vector)
		return true;

	return false;
}