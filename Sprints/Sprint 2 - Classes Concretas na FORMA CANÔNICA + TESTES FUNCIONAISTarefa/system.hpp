#ifndef System_HPP
#define System_HPP

#include <iostream>
#include <string>

using namespace std;

class System
{

public:
	/** System's default constructor */
	System (); 
	/** System's constructor */
	System (float, string);
	/** System's constructor of copy */
	System (System *system);
	/** System's destructor */
	virtual ~System(); 

	/** Set System's quantity */
	void setQuantity (float);
	/** Set System's id */
	void setId (string); 
	/** Get System's quantity */
	float getQuantity (); 
	/** Get System's Id */
	string getId (); 

	/** Increase quantity. */
	void increaseQuantity(float); 
	/** Decrease quantity. */
	void decreaseQuantity(float); 

protected:
	/** System's quantity*/ 
	float quantity; 
	/** System's Id*/ 
	string id; 

private:
	/** Operator overload */
	System& operator= (const System&); 
	/**  Operator == overload */
	bool operator== (const System&); 
};

#endif