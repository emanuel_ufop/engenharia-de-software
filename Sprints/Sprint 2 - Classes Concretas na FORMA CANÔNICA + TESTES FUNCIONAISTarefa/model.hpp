#ifndef MODEL_HPP
#define MODEL_HPP

#include <vector>
#include <iostream>
#include <iomanip>

#include "flow.hpp"

class Model
{

public:
	/** Model's constructor */
	Model(); 
	/** Model's constructor of copy*/
	Model(Model*);
	/** Model's destructor */
	virtual ~Model(); 

	/** Assings a Flow at a model */
	bool add (Flow*); 
	/** Assings a System at a model */
	bool add (System*); 
	/** Remove a Flow at a model */
	bool remove (Flow*); 
	/** Remove a System at a model */
	bool remove (System*); 
	/** Execute all flow equations on their respective systems. */
	void simulate (int, int); 
	/** Print the model's systems and flows. */
	void show (); 
	/** Give a report of the simulation */
	void report (); 

protected:
	
	vector<Flow*> flow_vector; /*!< Model's flows */ 
	vector<System*> system_vector; /*!< Model's systems */ 

private:
	/** operator = overload */
	Model& operator= (const Model&); 
	/** operator == overload */
	bool operator== (const Model&); 

};

#endif