#include "system.hpp"

using namespace std;

/**
  * \return System
  */
System::System ()
{
	this->quantity = 0;
	this->id[0] = '\0';
}

/**
  * \param quantuty is a quantity or energy to save
  * \param id is the id of the system
  * \return System
  */
System::System (float quantity, string id)
{
	this->quantity = quantity;
	this->id = id;
}

/**
  * \param system is a System, and the new one gonna be a copy of it
  * \return System
  */
System::System (System *system)
{
	this->quantity = system->quantity;
	this->id = system->id;
}

/**
  * \return
  */
System::~System ()
{

}

/**
  * \param quantity is the new value of quantity
  * \return void
  */
void System::setQuantity (float quantity)
{
	this->quantity = quantity;
}

/**
  * \param id is the new name of the system
  * \return void
  */
void System::setId (string id)
{
	this->id = id;
}

/**
  * \return float
  */
float System::getQuantity ()
{
	return this->quantity;
}

/**
  * \return string
  */
string System::getId ()
{
	return this->id;
}

/** \param value is the quantity that gonna be add to the system's quantity
  * \return void
  *
  **/
void System::increaseQuantity (float value)
{
	this->quantity += value;
}

/** \param value is the quantity that gonna be remove to the system's quantity
  * \return void
  *
  **/
void System::decreaseQuantity (float value)
{
	this->quantity -= value;
}

/**
  * \param system is the System that gonna be copied
  * \return System&
  */
System& System::operator= (const System &system)
{
	if(this == &system)
		return *this;

	this->id = system.id;
	this->quantity = system.quantity;

	return *this;
}

/**
  * \param system is the System that gonna be compared with the System that call this function
  * \return bool
  */
bool System::operator== (const System &system)
{
	if(this->id == system.id && this->quantity == system.quantity)
		return true;

	return false;
}