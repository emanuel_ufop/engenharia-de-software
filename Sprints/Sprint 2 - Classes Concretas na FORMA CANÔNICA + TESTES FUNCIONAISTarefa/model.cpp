#include "model.hpp"

using namespace std;

/**
  * \return Model
  */
Model::Model()
{
	this->flow_vector.clear();
	this->system_vector.clear();
}

/**
  * \param model is a Model, and the new one gonna be a copy of it
  * \return Model
  */
Model::Model(Model *model)
{
	flow_vector = model->flow_vector;
	system_vector = model->system_vector;
}

/**
  * \return Sem retorno
  */
Model::~Model ()
{

}

/** \param flow is the flow that gonna be added
  * \return bool
  **/
bool Model::add (Flow *flow)
{
	this->flow_vector.push_back(flow);
	return true;
}

/** \param system is the system that gonna be added
  * \return bool
  **/
bool Model::add (System *system)
{
	this->system_vector.push_back(system);
	return true;
}

/** \param flow is the flow that gonna be removed
  * \return bool
  **/
bool Model::remove (Flow *flow)
{
	int size = flow_vector.size();

	for(int i = 0; i < size; i++)
	{
		if(this->flow_vector[i]->getId() == flow->getId())
		{
			this->flow_vector.erase(this->flow_vector.begin() + i);
			return true;
		}
	}

	return false; // the flow didn't exist
}

/** \param system is the system that gonna be removed
  * \return bool
  **/
bool Model::remove (System *system)
{
	int size = system_vector.size();
	 
	for(int i = 0; i < size; i++)
	{
		if(this->system_vector[i]->getId() == system->getId())
		{
			this->system_vector.erase(this->system_vector.begin() + i);
			return true;
		}
	}

	return false; // the system didn't exist 
}

/** \param begin is the begin of the loop
  * \param end is the end of the loop
  * \return void
  *
  **/
void Model::simulate (int begin, int end)
{
	int size = this->flow_vector.size(); // get a vector size
	float *v = new float[size]; // alocate a float vector

	for(int i = begin; i < end; i++)
	{
		for(int j = 0; j < size; j++)
			v[j] = this->flow_vector[j]->execute(); // save the results of executes

		for(int k = 0; k < size; k++)
		{
			// set the new systens' value
			if(this->flow_vector[k]->getTarget() != NULL)
				this->flow_vector[k]->getTarget()->increaseQuantity(v[k]);

			if(this->flow_vector[k]->getSource() != NULL)
				this->flow_vector[k]->getSource()->decreaseQuantity(v[k]);		
		}
	}

}

/**
  * \return void
  **/
void Model::show ()
{
	for(vector<System*>::iterator it = this->system_vector.begin(); it != system_vector.end(); it++)
	{
		cout << "System: " << (*it)->getId() << "   Quantity: " << (*it)->getQuantity() << endl;
	}

	for(vector<Flow*>::iterator it = this->flow_vector.begin(); it != flow_vector.end(); it++)
	{
		cout << "Flow " << (*it)->getId() << " -> Source: ";
		if((*it)->getSource() != NULL)
			cout << (*it)->getSource()->getId();
		else
			cout << fixed << setw(10) << "NULL";

		cout << "   Target: ";
		if((*it)->getTarget() != NULL)
			cout << (*it)->getTarget()->getId();
		else
			cout << "NULL";

		cout << endl;
	}
}

/**
  * \return void
  *
  **/
void report ()
{

}

/**
  * \param model is a model that gonna be copied
  * \return Model&
  */
Model& Model::operator= (const Model &model)
{
	if(this == &model)
		return *this;


	for(vector<Flow*>::iterator it = this->flow_vector.begin(); it != this->flow_vector.end(); it++)
		delete *it;

	this->flow_vector.clear();

	for(vector<System*>::iterator it = this->system_vector.begin(); it != this->system_vector.end(); it++)
		delete *it;

	this->system_vector.clear();


	for(int i = 0, size = model.flow_vector.size(); i < size; i++)
	{
		Flow *aux = new Flow;
		aux = model.flow_vector[i];
		this->flow_vector.push_back(aux);
	}

	for(int i = 0, size = model.system_vector.size(); i < size; i++)
	{
		System *aux = new System;
		aux = model.system_vector[i];
		this->system_vector.push_back(aux);
	}

	/*for(vector<Flow>::iterator it = model.flow_vector.begin(); it != model.flow_vector.end(); it++)
		this->flow_vector.push_back(*it);

	for(vector<System>::iterator it = model.system_vector.begin(); it != model.system_vector.end(); it++)
		this->system_vector.push_back(*it);*/

	return *this;
}

/**
  * \param model is a model that gonna be compared with the Model that call this function
  * \return bool
  */
bool Model::operator== (const Model &model)
{
	if(this == &model) 
		return true;

	if(this->flow_vector.size() != model.flow_vector.size() || this->system_vector.size() != model.system_vector.size())
		return false;

	for(int i = 0, size = model.flow_vector.size(); i < size; i++)
		if(!(this->flow_vector.size() == model.flow_vector.size()))
			return false;

	for(int i = 0, size = model.system_vector.size(); i < size; i++)
		if(!(this->system_vector.size() == model.system_vector.size()))	
			return false;

	return true;
}