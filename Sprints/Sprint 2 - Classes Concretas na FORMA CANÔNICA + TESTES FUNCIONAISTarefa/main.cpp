/**
* @file main.cpp
* @author Emanuel Jesus Xavier
* @date September 17, 2019
* @brief This program simulates a dynamics systems
*
*/

/*! \mainpage My Vensim
 *
 * \section intro_sec Introduction
 *
 * http://www.terralab.ufop.br/dokuwiki/lib/exe/fetch.php?media=terralab:curso:tecprog:trabalho_engenhariadesoftware.pdf
 *
 */

#include <iostream>
#include <string>
#include <math.h>
#include <assert.h>

#include "model.hpp"

using namespace std;

class myFlow1 : public Flow
{

public:

	~myFlow1()
	{

	}

	float execute ()
	{
		float quantity = this->source->getQuantity();

		quantity = quantity * 0.01;

		return quantity;
	}
};

class myFlow2 : public Flow
{

public:

	~myFlow2()
	{

	}

	float execute ()
	{
		float quantity = this->target->getQuantity();

		quantity = 0.01 * quantity * (1 - quantity / 70); 

		return quantity;
	}
};

void execute1 ();
void execute2 ();
void execute3 ();

int main()
{
	execute1();
	execute2();
	execute3();

	return 0;
}
/** This function make a functional test 1
  *	\param
  * \return void
  *
  */
void execute1 ()
{
	System *pop1 = new System, *pop2 = new System;
	Model *m1 = new Model;
	myFlow1 *exponencial = new myFlow1/*("exponencial", pop1, pop2)*/;

	m1->add(exponencial);
	m1->add(pop1);
	m1->add(pop2);

	exponencial->setId("exponencial");
	pop1->setId("pop1");
	pop2->setId("pop2");

	pop1->setQuantity(100);
	pop2->setQuantity(0);

	exponencial->connect(pop1, 0); exponencial->connect(pop2, 1);

	m1->simulate(0, 100);

	//m1->show();

	assert(fabs(pop1->getQuantity() - 36.6032) < 0.0001);
	assert(fabs(pop2->getQuantity() - 63.3968) < 0.0001);
}
/** This function make a functional test 2
  *	\param
  * \return void
  *
  */
void execute2 ()
{
	Model *m2 = new Model;
	myFlow2 *logistica = new myFlow2;
	System *p1 = new System, *p2 = new System;


	m2->add(logistica);

	m2->add(p1);
	m2->add(p2);

	p1->setId("p1");
	p2->setId("p2");

	logistica->setId("logistica");

	p1->setQuantity(100);
	p2->setQuantity(10);

	logistica->connect(p1, 0); logistica->connect(p2, 1);

	Model *m = new Model;
	m = m2;
	m->simulate(0, 100);

	//m2->show();

	assert(fabs(p1->getQuantity() - 88.2167) < 0.0001);
	assert(fabs(p2->getQuantity() - 21.7833) < 0.0001);
}
/** This function make a functional test 3
  *	\param
  * \return void
  *
  */
void execute3 ()
{
	Model *m3 = new Model;
	myFlow1 *v = new myFlow1, *f = new myFlow1, *g = new myFlow1, *t = new myFlow1, *u = new myFlow1, *r = new myFlow1;
	System *Q1 = new System, *Q2 = new System, *Q3 = new System, *Q4 = new System, *Q5 = new System;

	m3->add(f);
	m3->add(g);
	m3->add(t);
	m3->add(u);
	m3->add(r);
	m3->add(v);
	m3->add(Q1);
	m3->add(Q2);
	m3->add(Q3);
	m3->add(Q4);
	m3->add(Q5);

	v->setId("v");
	f->setId("f");
	g->setId("g");
	t->setId("t");
	u->setId("u");
	r->setId("r");

	Q1->setId("Q1");
	Q2->setId("Q2");
	Q3->setId("Q3");
	Q4->setId("Q4");
	Q5->setId("Q5");

	Q1->setQuantity(100);
	Q2->setQuantity(0);
	Q3->setQuantity(100);
	Q4->setQuantity(0);
	Q5->setQuantity(0);

	v->connect(Q4, 0); v->connect(Q1, 1);
	f->connect(Q1, 0); f->connect(Q2, 1);
	g->connect(Q1, 0); g->connect(Q3, 1);
	t->connect(Q2, 0); t->connect(Q3, 1);
	u->connect(Q3, 0); u->connect(Q4, 1);
	r->connect(Q2, 0); r->connect(Q5, 1);

	Model *m = new Model(m3);

	delete m3;

	m->simulate(0, 100);

	//m->show();

	assert(fabs(Q1->getQuantity() - 31.8513) < 0.0001);
	assert(fabs(Q2->getQuantity() - 18.4003) < 0.0001);
	assert(fabs(Q3->getQuantity() - 77.1143) < 0.0001);
	assert(fabs(Q4->getQuantity() - 56.1728) < 0.0001);
	assert(fabs(Q5->getQuantity() - 16.4612) < 0.0001);
}