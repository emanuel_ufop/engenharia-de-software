#include "flow.hpp"

/**
  * \return Flow
  */
Flow::Flow () 
{
	this->source = NULL;
	this->target = NULL;
}

/**
  * \param id is the flow's name
  * \param source is the System that gonna give energy
  * \param target is the System that gonna recive the energy
  * \return Flow
  */
Flow::Flow (string id, System *source, System *target)
{
	this->source = source;
	this->target = target;
	this->id = id;
}

/**
  * \param flow is a Flow, and the new one gonna be a copy of it
  * \return Flow
  */
Flow::Flow(Flow *flow)
{
	this->source = flow->source;
	this->target = flow->target;
	this->id = flow->id;
}

/**
  * \return
  */
Flow::~Flow()
{

}

/**
  * \param id is the Flow's name
  * \return void
  */
void Flow::setId (string id)
{
	this->id = id;
}

/**
  * \return string
  */
string Flow::getId()
{
	return this->id;
}

/**
  * \return System*
  */
System* Flow::getSource()
{
	return this->source;
}

/**
  * \return System*
  */
System* Flow::getTarget()
{
	return this->target;
}

/** \param s is the system that gonna be connected
  * \param p define if is a target or a source
  * \return bool
  *
  **/
bool Flow::connect (System *s, int p)
{
	if(s != NULL)
	{
		if(p <= 0) // connect the source
		{
			if(this->source == NULL)
			{
				this->source = s;
				return 0; // successfull connection
			}
			else
				return 1; // source already connected
		}

		else // connect the target
		{
			if(this->target == NULL)
			{
				this->target = s;
				return 0; // successfull connection
			}
			else
				return 1; // target already connected
		}
	}

	return -1; // system == NULL
}

/** \param p which system gonna be disconnected, target or source
  * \return bool
  *
  **/
bool Flow::disconnect (int p)
{
	if(p <= 0) // disconnect the source
	{
		if(this->source != NULL)
		{
			this->source = NULL;
			return true;
		}
	}

	else if(this->target != NULL) // disconnect the target
	{
		this->target = NULL;		
		return true;
	}

	return false; // the flow is already disconnected
}

/**
  * \return void
  *
  **/
float Flow::execute ()
{
	return 0;
}

/**
  * \param flow is a flow that gonna be copied
  * \return Flow&
  */
Flow& Flow::operator= (const Flow &flow)
{
	if(this == &flow)
		return *this;

	this->id = flow.id;

	this->source = flow.source;
	this->target = flow.target;

	return *this;
}

/**
  * \param flow is a flow that gonna be compared with the Flow that call this function
  * \return bool
  */
bool Flow::operator== (const Flow &flow)
{
	if(this->id == flow.id && this->source == flow.source && this->target == flow.target)
		return true;

	return false;
}