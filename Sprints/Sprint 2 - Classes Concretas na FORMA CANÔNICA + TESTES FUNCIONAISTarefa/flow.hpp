#ifndef FLOW_HPP
#define FLOW_HPP

#include <iostream>
#include <string>

#include "System.hpp"

using namespace std;

class Flow
{

public:
	/** Default Flow's constructor */
	Flow ();
	/** Flow's constructor */
	Flow (string, System*, System*);
	/** Flow's constructor of copy */
	Flow (Flow*);
	/** Flow's destructor */
	virtual ~Flow(); 
	/** Set id */
	void setId (string); 
	/** Get Flow's id */
	string getId(); 
	/** Get Flow'd source */
	System* getSource(); 
	/** Get Flow's target */
	System* getTarget(); 

	/** Connect one System to the Flow. */
	bool connect (System*, int);
	/** Disconnect one Flow's system. */				 			 
	bool disconnect (int); 
	/** Define the equation to the Flow */
	virtual float execute (); 

protected:
	/** The system that gonna recive the quantity */
	System *target; 
	/** The system that gonna give the quantity */ 
	System *source; 
	/** Flow's Id */ 
	string id; 

private:
	/** operator = overload */
	Flow& operator= (const Flow&); 
	/** operator == overload */
	bool operator== (const Flow&); 
};

#endif