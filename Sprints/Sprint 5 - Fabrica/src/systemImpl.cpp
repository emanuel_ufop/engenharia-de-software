#include "systemImpl.hpp"

using namespace std;

/**
  * \return SystemImpl
  */
SystemImpl::SystemImpl ()
{
	this->quantity = 0;
	this->id[0] = '\0';
}

/**
  * \param quantuty is a quantity or energy to save
  * \param id is the id of the SystemImpl
  * \return SystemImpl
  */
SystemImpl::SystemImpl (float quantity, string id)
{
	this->quantity = quantity;
	this->id = id;
}

/**
  * \param SystemImpl is a SystemImpl, and the new one gonna be a copy of it
  * \return SystemImpl
  */
SystemImpl::SystemImpl (SystemImpl *systemImpl)
{
	this->quantity = systemImpl->quantity;
	this->id = systemImpl->id;
}

/**
  * \return
  */
SystemImpl::~SystemImpl ()
{

}

/**
  * \param quantity is the new value of quantity
  * \return void
  */
void SystemImpl::setQuantity (float quantity)
{
	this->quantity = quantity;
}

/**
  * \param id is the new name of the SystemImpl
  * \return void
  */
void SystemImpl::setId (string id)
{
	this->id = id;
}

/**
  * \return float
  */
float SystemImpl::getQuantity ()
{
	return this->quantity;
}

/**
  * \return string
  */
string SystemImpl::getId ()
{
	return this->id;
}

/** \param value is the quantity that gonna be add to the SystemImpl's quantity
  * \return void
  *
  **/
void SystemImpl::increaseQuantity (float value)
{
	this->quantity += value;
}

/** \param value is the quantity that gonna be remove to the SystemImpl's quantity
  * \return void
  *
  **/
void SystemImpl::decreaseQuantity (float value)
{
	this->quantity -= value;
}

/**
  * \param SystemImpl is the SystemImpl that gonna be copied
  * \return SystemImpl&
  */
SystemImpl& SystemImpl::operator= (const SystemImpl &systemImpl)
{
	if(this == &systemImpl)
		return *this;

	this->id = systemImpl.id;
	this->quantity = systemImpl.quantity;

	return *this;
}

/**
  * \param SystemImpl is the SystemImpl that gonna be compared with the SystemImpl that call this function
  * \return bool
  */
bool SystemImpl::operator== (const SystemImpl &systemImpl)
{
  if(this == &systemImpl)
    return true; 
  
	if(this->id == systemImpl.id && this->quantity == systemImpl.quantity)
		return true;

	return false;
}