#ifndef systemImpl_HPP
#define systemImpl_HPP

#include <iostream>
#include <string>

#include "system.hpp"

using namespace std;

class SystemImpl : public System
{

public:
	/** SystemImpl's default constructor */
	SystemImpl (); 
	/** SystemImpl's constructor */
	SystemImpl (float, string);
	/** SystemImpl's constructor of copy */
	SystemImpl (SystemImpl *SystemImpl);
	/** SystemImpl's destructor */
	virtual ~SystemImpl(); 

	/** Set SystemImpl's quantity */
	void setQuantity (float);
	/** Set SystemImpl's id */
	void setId (string); 
	/** Get SystemImpl's quantity */
	float getQuantity (); 
	/** Get SystemImpl's Id */
	string getId (); 

	/** Increase quantity. */
	void increaseQuantity(float); 
	/** Decrease quantity. */
	void decreaseQuantity(float); 

protected:
	/** SystemImpl's quantity*/ 
	float quantity; 
	/** SystemImpl's Id*/ 
	string id; 

private:
	/** Operator overload */
	SystemImpl& operator= (const SystemImpl&); 
	/**  Operator == overload */
	bool operator== (const SystemImpl&); 
};

#endif