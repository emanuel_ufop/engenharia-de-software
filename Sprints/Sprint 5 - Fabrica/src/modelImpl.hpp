#ifndef modelImpl_HPP
#define modelImpl_HPP

#include <vector>
#include <iostream>
#include <iomanip>

#include "flowImpl.hpp"
#include "SystemImpl.hpp"
#include "model.hpp"


class ModelImpl : public Model
{

public:
	/** ModelImpl's constructor */
	ModelImpl(); 
	/** ModelImpl's constructor of copy*/
	ModelImpl(ModelImpl*);
	/** ModelImpl's destructor */
	virtual ~ModelImpl(); 

	/** Tell if a there is a system or not in the model */
	bool searchSystem(string);
	/** Tell if a there is a flow or not in the model */
	bool searchFlow (string id);
	/** Return a dystem */
	System* findSystem(string);
	/** Return a flow*/
	Flow* findFlow(string);
	/** Create and add a new System  */
	System* createSystem();
	/** Create and add a new System  */
	System* createSystem(string, float);
	/** Create and add a new Flow */
	Flow* createFlow (string, System*, System*);
	/** Remove a FlowImpl at a ModelImpl */
	bool remove (Flow*); 
	/** Remove a SystemImpl at a ModelImpl */
	bool remove (System*); 
	/** Connect a flow and a system */
	bool connect(string, string, int);
	/** Execute all flowImpl equations on their respective systemImpls. */
	void simulate (int, int); 
	/** Print the ModelImpl's systemImpls and flowImpls. */
	void show (); 
	/** Give a report of the simulation */
	void report (); 
	/** Create a new Model */
	static Model* createModel();

protected:
	
	vector<Flow*> flow_vector; /*!< ModelImpl's flowImpls */ 
	vector<System*> system_vector; /*!< ModelImpl's systemImpls */
	
private:
	/** Assings a FlowImpl at a ModelImpl */
	bool add (Flow*); 
	/** Assings a SystemImpl at a ModelImpl */
	bool add (System*); 

	/** operator = overload */
	ModelImpl& operator= (const ModelImpl&); 
	/** operator == overload */
	bool operator== (const ModelImpl&); 

};

#endif