#ifndef flow_HPP
#define flow_HPP

#include <iostream>
#include <string>

#include "system.hpp"

using namespace std;

class Flow
{

public:
	/** Set id */
	virtual void setId (string) = 0; 
	/** Get Flow's id */
	virtual string getId() = 0; 
	/** Get Flow'd source */
	virtual System* getSource() = 0; 
	/** Get Flow's target */
	virtual System* getTarget() = 0; 

	/** Connect one System to the Flow. */
	virtual bool connect (System*, int) = 0;
	/** Disconnect one Flow's system. */				 			 
	virtual bool disconnect (int) = 0; 
	/** Define the equation to the Flow */
	virtual float execute (float, float) = 0; 
};

#endif