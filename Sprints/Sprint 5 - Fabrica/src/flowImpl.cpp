#include "flowImpl.hpp"

/**
  * \return FlowImpl
  */
FlowImpl::FlowImpl () 
{
	source = NULL;
	target = NULL;
	id = "";
}

/**
  * \param id is the flowImpl's name
  * \param source is the System that gonna give energy
  * \param target is the System that gonna recive the energy
  * \return FlowImpl
  */
FlowImpl::FlowImpl (string id, System *source, System *target)
{
	source = source;
	target = target;
	id = id;
}

/**
  * \param flowImpl is a FlowImpl, and the new one gonna be a copy of it
  * \return FlowImpl
  */
FlowImpl::FlowImpl(FlowImpl *flowImpl)
{
	source = flowImpl->source;
	target = flowImpl->target;
	id = flowImpl->id;
}

/**
  * \return
  */
FlowImpl::~FlowImpl()
{

}

/**
  * \param id is the FlowImpl's name
  * \return void
  */
void FlowImpl::setId (string id)
{
	this->id = id;
}

/**
  * \return string
  */
string FlowImpl::getId()
{
	return id;
}

/**
  * \return System*
  */
System* FlowImpl::getSource()
{
	return source;
}

/**
  * \return System*
  */
System* FlowImpl::getTarget()
{
	return target;
}

/** \param s is the System that gonna be connected
  * \param p define if is a target or a source
  * \return bool
  *
  **/
bool FlowImpl::connect (System *s, int p)
{
	if(s != NULL)
	{
		if(p <= 0) // connect the source
		{
			if(source == NULL)
			{
				source = s;
				return 0; // successfull connection
			}
			else
				return 1; // source already connected
		}

		else // connect the target
		{
			if(target == NULL)
			{
				target = s;
				return 0; // successfull connection
			}
			else
				return 1; // target already connected
		}
	}

	return -1; // System == NULL
}

/** \param p which System gonna be disconnected, target or source
  * \return bool
  *
  **/
bool FlowImpl::disconnect (int p)
{
	if(p <= 0) // disconnect the source
	{
		if(this->source != NULL)
		{
			source = NULL;
			return true;
		}
	}

	else if(this->target != NULL) // disconnect the target
	{
		target = NULL;		
		return true;
	}

	return false; // the flowImpl is already disconnected
}

/**
  * \param source is the quantity of the FlowImpl's source
  *
  * \param target is the quantity of the FlowImpl's target
  *
  * \return float
  *
  **/
float FlowImpl::execute (float source, float target)
{
	return 0;
}

/**
  * \param flowImpl is a flowImpl that gonna be copied
  * \return FlowImpl&
  */
FlowImpl& FlowImpl::operator= (const FlowImpl &flowImpl)
{
	if(this == &flowImpl)
		return *this;

	id = flowImpl.id;

	source = flowImpl.source;
	target = flowImpl.target;

	return *this;
}

/**
  * \param flowImpl is a flowImpl that gonna be compared with the FlowImpl that call this function
  * \return bool
  */
bool FlowImpl::operator== (const FlowImpl &flowImpl)
{
	if(this == &flowImpl)
		return true;

	if(id == flowImpl.id && target == flowImpl.target && source == flowImpl.source)
		return true;

	return false;
}