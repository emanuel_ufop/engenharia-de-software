#ifndef flowImpl_HPP
#define flowImpl_HPP

#include <iostream>
#include <string>

#include "system.hpp"
#include "flow.hpp"

using namespace std;

class FlowImpl : public Flow
{

public:
	/** Default Flow's constructor */
	FlowImpl ();
	/** Flow's constructor */
	FlowImpl (string, System*, System*);
	/** Flow's constructor of copy */
	FlowImpl (FlowImpl*);
	/** Flow's destructor */
	virtual ~FlowImpl(); 
	/** Set id */
	void setId (string); 
	/** Get Flow's id */
	string getId(); 
	/** Get Flow'd source */
	System* getSource(); 
	/** Get Flow's target */
	System* getTarget(); 

	/** Connect one SystemImpl to the Flow. */
	bool connect (System*, int);
	/** Disconnect one Flow's systemImpl. */				 			 
	bool disconnect (int); 
	/** Define the equation to the Flow */
	virtual float execute (float, float); 

protected:
	/** The systemImpl that gonna recive the quantity */
	System *target; 
	/** The systemImpl that gonna give the quantity */ 
	System *source; 
	/** Flow's Id */ 
	string id; 

private:
	/** operator = overload */
	FlowImpl& operator= (const FlowImpl&); 
	/** operator == overload */
	bool operator== (const FlowImpl&); 
};

#endif