#include "unitTestSystem.hpp"

void unitTestSystem_constructorSystem1 ()
{
	SystemImpl *system = new SystemImpl();

	assert(fabs(system->getQuantity() - 0) < 0.0001);
	assert(system->getId() == "");

	delete system;
}

void unitTestSystem_constructorSystem2 ()
{
	SystemImpl *system = new SystemImpl(1.234, "mySystem");

	assert(fabs(system->getQuantity() - 1.234) < 0.0001);
	assert(system->getId() == "mySystem");

	delete system;
}

void unitTestSystem_constructorSystem3 ()
{
	SystemImpl *system1 = new SystemImpl(1.234, "mySystem");
	SystemImpl *system2 = new SystemImpl(system1);

	assert(fabs(system2->getQuantity() - 1.234) < 0.0001);
	assert(system2->getId() == "mySystem");

	delete system1;
	delete system2;
}

void unitTestSystem_increaseQuantity ()
{
	SystemImpl *system = new SystemImpl();

	system->increaseQuantity(1.01);

	assert(fabs(system->getQuantity() - 1.01) < 0.0001);

	delete system;
}

void unitTestSystem_decreaseQuantity ()
{
	SystemImpl *system = new SystemImpl(1.234, "mySystem");

	system->decreaseQuantity(1.234);

	assert(fabs(system->getQuantity() - 0) < 0.0001);

	delete system;
}

void unitTestSystem_operator_attribution ()
{
	SystemImpl *system1 = new SystemImpl(1.234, "mySystem");
	SystemImpl *system2 = new SystemImpl();

	system2 = system1;

	assert(fabs(system2->getQuantity() - 1.234) < 0.0001);
	assert(system2->getId() == "mySystem");

	delete system1;
	delete system2;
}

void unitTestSystem_operator_compare ()
{
	SystemImpl *system1 = new SystemImpl(1.234, "mySystem");
	SystemImpl *system2 = new SystemImpl(system1);

	assert(system1 == system2);
}

void unitTestSystem_global ()
{
	unitTestSystem_constructorSystem1 ();
	unitTestSystem_constructorSystem2 ();
	unitTestSystem_constructorSystem3 ();
	unitTestSystem_increaseQuantity ();
	unitTestSystem_decreaseQuantity ();
	unitTestSystem_operator_attribution ();
	unitTestSystem_operator_compare ();
}