#include "unitTestModel.hpp"

class myFlow1 : public FlowImpl
{
	float execute (float source, float target)
	{
		return source * 0.01;
	}
};

void unitTestModel_constructor ()
{
	Model *m = Model::createModel();

	assert(m != nullptr);
}

void unitTestModel_add_flow () 
{
	Model *m = Model::createModel();

	m->createSystem("s1", 0);
	m->createSystem("s2", 0);
	m->createSystem("s3", 0);
	m->createSystem("s4", 0);

	m->createFlow<myFlow1>("f1", m->findSystem("s1"), m->findSystem("s2"));
	m->createFlow<myFlow1>("f2", m->findSystem("s2"), m->findSystem("s3"));
	m->createFlow<myFlow1>("f3", m->findSystem("s4"), m->findSystem("s1"));

	assert(m->searchFlow("f1"));
	assert(m->searchFlow("f2"));
	assert(m->searchFlow("f3"));
}

void unitTestModel_add_system () 
{
	Model *m = Model::createModel();

	m->createSystem("s1", 0);
	m->createSystem("s2", 0);
	m->createSystem("s3", 0);
	m->createSystem("s4", 0);

	assert(m->searchSystem("s1"));
	assert(m->searchSystem("s2"));
	assert(m->searchSystem("s3"));
	assert(m->searchSystem("s4"));
}

void unitTestModel_remove_flow () 
{
	Model *m = Model::createModel();

	m->createSystem("s1", 0);
	m->createSystem("s2", 0);
	m->createSystem("s3", 0);
	m->createSystem("s4", 0);

	m->createFlow<myFlow1>("f1", m->findSystem("s1"), m->findSystem("s2"));
	m->createFlow<myFlow1>("f2", m->findSystem("s2"), m->findSystem("s3"));
	m->createFlow<myFlow1>("f3", m->findSystem("s4"), m->findSystem("s1"));

	m->remove(m->findFlow("f1"));
	m->remove(m->findFlow("f3"));

	assert(!m->searchFlow("f1"));
	assert(m->searchFlow("f2"));
	assert(!m->searchFlow("f3"));
}

void unitTestModel_remove_system () 
{
	Model *m = Model::createModel();

	m->createSystem("s1", 0);
	m->createSystem("s2", 0);
	m->createSystem("s3", 0);
	m->createSystem("s4", 0);

	m->show();
	m->remove(m->findSystem("s1"));
	m->remove(m->findSystem("s3"));

	//m->show();
	
	assert(!m->searchFlow("s1"));
	assert(m->findSystem("s2") != nullptr);
	assert(!m->searchFlow("s3"));
	assert(m->findSystem("s4") != nullptr);
}

void unitTestModel_simulate () 
{
	Model *m = Model::createModel();

	m->createSystem("Q1", 100);
	m->createSystem("Q2", 0);
	m->createSystem("Q3", 100);
	m->createSystem("Q4", 0);
	m->createSystem("Q5", 0);


	m->createFlow<myFlow3>("v", m->findSystem("Q4"), m->findSystem("Q1"));
	m->createFlow<myFlow3>("f", m->findSystem("Q1"), m->findSystem("Q2"));
	m->createFlow<myFlow3>("g", m->findSystem("Q1"), m->findSystem("Q3"));
	m->createFlow<myFlow3>("t", m->findSystem("Q2"), m->findSystem("Q3"));
	m->createFlow<myFlow3>("u", m->findSystem("Q3"), m->findSystem("Q4"));
	m->createFlow<myFlow3>("r", m->findSystem("Q2"), m->findSystem("Q5"));


	m->simulate(0, 100);
	//m->show();

	assert(fabs(m->findSystem("Q1")->getQuantity() - 31.8513) < 0.0001);
	assert(fabs(m->findSystem("Q2")->getQuantity() - 18.4003) < 0.0001);
	assert(fabs(m->findSystem("Q3")->getQuantity() - 77.1143) < 0.0001);
	assert(fabs(m->findSystem("Q4")->getQuantity() - 56.1728) < 0.0001);
	assert(fabs(m->findSystem("Q5")->getQuantity() - 16.4612) < 0.0001);
}

void unitTestModel_global ()
{
	unitTestModel_constructor ();
	unitTestModel_add_flow (); 
	unitTestModel_add_system (); 
	unitTestModel_remove_flow (); 
	unitTestModel_remove_system ();
	unitTestModel_simulate ();
	unitTestModel_operator_attribution ();
	unitTestModel_operator_compare ();
}
