#include "functionalTest3.hpp"

/**
  * \param source is the quantity of the Flow's source
  *
  * \param target is the quantity of the Flow's target
  *
  * \return float
  *
  **/
float myFlow3::execute (float source, float target)
{
	return source * 0.01;
}

/**
  * \return void
  */
void test3 ()
{
	Model *m = Model::createModel();

	m->createSystem("Q1", 100);
	m->createSystem("Q2", 0);
	m->createSystem("Q3", 100);
	m->createSystem("Q4", 0);
	m->createSystem("Q5", 0);


	m->createFlow<myFlow3>("v", m->findSystem("Q4"), m->findSystem("Q1"));
	m->createFlow<myFlow3>("f", m->findSystem("Q1"), m->findSystem("Q2"));
	m->createFlow<myFlow3>("g", m->findSystem("Q1"), m->findSystem("Q3"));
	m->createFlow<myFlow3>("t", m->findSystem("Q2"), m->findSystem("Q3"));
	m->createFlow<myFlow3>("u", m->findSystem("Q3"), m->findSystem("Q4"));
	m->createFlow<myFlow3>("r", m->findSystem("Q2"), m->findSystem("Q5"));


	m->simulate(0, 100);
	m->show();

	assert(fabs(m->findSystem("Q1")->getQuantity() - 31.8513) < 0.0001);
	assert(fabs(m->findSystem("Q2")->getQuantity() - 18.4003) < 0.0001);
	assert(fabs(m->findSystem("Q3")->getQuantity() - 77.1143) < 0.0001);
	assert(fabs(m->findSystem("Q4")->getQuantity() - 56.1728) < 0.0001);
	assert(fabs(m->findSystem("Q5")->getQuantity() - 16.4612) < 0.0001);
}