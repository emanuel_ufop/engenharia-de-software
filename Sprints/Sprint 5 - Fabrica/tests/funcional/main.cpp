/**
* @file main.cpp
* @author Emanuel Jesus Xavier
* @date September 14, 2019
* @brief This program is a functional test for a dynamics system simulation
*
*/

#include "functionalTest1.hpp"
#include "functionalTest2.hpp"
#include "functionalTest3.hpp"

using namespace std;

int main()
{
	test1();
	test2();
	test3();

	return 0;
}