#include "functionalTest1.hpp"

/**
  * \param source is the quantity of the Flow's source
  *
  * \param target is the quantity of the Flow's target
  *
  * \return float
  *
  **/
float myFlow1::execute (float source, float target)
{
	return source * 0.01;
}

/**
  * \return void
  */

void test1()
{
	Model *m = Model::createModel();

	m->createSystem("pop1", 100);
	m->createSystem("pop2", 0);

	m->createFlow<myFlow1>("exponencial", m->findSystem("pop1"), m->findSystem("pop2"));
	m->connect("exponencial", "pop1", 0);
	m->connect("exponencial", "pop2", 1);

	m->simulate(0, 100);
	//m->show();

	assert(fabs(m->findSystem("pop1")->getQuantity() - 36.6032) < 0.0001);
	assert(fabs(m->findSystem("pop2")->getQuantity() - 63.3968) < 0.0001);
}