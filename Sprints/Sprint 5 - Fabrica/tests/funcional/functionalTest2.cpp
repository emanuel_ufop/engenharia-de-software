#include "functionalTest2.hpp"

/**
  * \param source is the quantity of the Flow's source
  *
  * \param target is the quantity of the Flow's target
  *
  * \return float
  *
  **/
float myFlow2::execute (float source, float target)
{
	return 0.01 * target * (1 - target / 70);
}

/**
  * \return void
  */
void test2 ()
{
	Model *m = Model::createModel();

	m->createSystem("p1", 100);
	m->createSystem("p2", 10);

	m->createFlow<myFlow2>("logistica", m->findSystem("p1"), m->findSystem("p2"));
	m->connect("logistica", "p1", 0);
	m->connect("logistica", "p2", 1);

	m->simulate(0, 100);
	//m->show();

	assert(fabs(m->findSystem("p1")->getQuantity() - 88.2167) < 0.0001);
	assert(fabs(m->findSystem("p2")->getQuantity() - 21.7833) < 0.0001);
}