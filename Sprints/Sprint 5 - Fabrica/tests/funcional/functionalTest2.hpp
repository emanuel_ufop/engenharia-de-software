#ifndef functionalTest2_HPP
#define functionalTest2_HPP

#include <math.h>
#include <assert.h>

#include "../../src/model.hpp"
#include "../../src/flowImpl.hpp"


using namespace std;

class myFlow2 : public FlowImpl
{
	/** Define the equation to the Flow */
	float execute (float, float);
};

/** This is a functional test*/
void test2 ();

#endif